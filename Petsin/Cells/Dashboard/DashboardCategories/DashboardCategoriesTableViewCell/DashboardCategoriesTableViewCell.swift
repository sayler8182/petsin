//
//  DashboardCategoriesTableViewCell.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardCategoriesTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardCategoriesTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 150
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            DashboardCategoryCollectionViewCell.register(collectionView: self.collectionView)
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    weak var delegate: DashboardCategoriesTableViewCellDelegate?
    var categories: [Category] = [] {
        didSet { self.collectionView.reloadData() }
    }
    var categories_selected: [Category] = [] {
        didSet { self.collectionView.reloadData() }
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
extension DashboardCategoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // cell count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    // cell click
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        let category = self.categories[indexPath.row]
        
        // delegate
        self.delegate?.dashboardCategoriesTableViewCellSelected(cell: self, category: category)
    }
    
    // cell view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: DashboardCategoryCollectionViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let category = self.categories[indexPath.row]
        let cell = cell as! DashboardCategoryCollectionViewCell
        cell.category = category
        cell.isCategorySelected = self.categories_selected.contains(where: { $0.id == category.id })
    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return DashboardCategoryCollectionViewCell.getSize(collectionSize: collectionView.frame.size)
    }
}
