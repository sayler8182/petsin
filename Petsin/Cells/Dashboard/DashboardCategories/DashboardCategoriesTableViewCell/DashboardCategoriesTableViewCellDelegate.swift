//
//  DashboardCategoriesTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardCategoriesTableViewCellDelegate: class {
    func dashboardCategoriesTableViewCellSelected(cell: DashboardCategoriesTableViewCell, category: Category)
}
