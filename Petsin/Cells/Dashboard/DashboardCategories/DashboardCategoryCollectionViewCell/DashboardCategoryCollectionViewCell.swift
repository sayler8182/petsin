//
//  DashboardCategoryCollectionViewCell.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardCategoryCollectionViewCell: UICollectionViewCellBase {
    override class var reuseIdentifier: String { return "DashboardCategoryCollectionViewCell" }
    
    override class func getSize(collectionSize: CGSize) -> CGSize {
        let size: CGFloat = 100
        return CGSize(width: size, height: size)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    fileprivate weak var selectionLayer: CALayer? = nil
    
    var category: Category? {
        didSet {
            self.titleLabel.text = self.category?.name
        }
    }
    var isCategorySelected: Bool = false {
        didSet { self.setNeedsDisplay() }
    }
    
    // draw
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.layer.shadow(radius: 3, opacity: 0.2)
        
        // update selection
        self.updateSelection()
    }
    
    // update selection
    fileprivate func updateSelection() {
        self.selectionLayer?.removeFromSuperlayer()
        guard self.isCategorySelected == true else { return }
        let cellSize: CGFloat = self.frame.size.height
        let size: CGFloat = 40
        let layer = CAShapeLayer()
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: cellSize - size, y:0))
        path.addLine(to: CGPoint(x: cellSize, y: 0))
        path.addLine(to: CGPoint(x: cellSize, y: size))
        path.addLine(to: CGPoint(x: cellSize - size, y: 0))
        path.close()
        
        layer.path = path.cgPath
        layer.fillColor = Resources.lightGreen.cgColor
        self.layer.addSublayer(layer)
        self.selectionLayer = layer
    }
}


