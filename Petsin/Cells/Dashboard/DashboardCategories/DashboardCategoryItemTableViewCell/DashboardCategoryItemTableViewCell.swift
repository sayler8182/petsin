//
//  DashboardCategoryItemTableViewCell.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardCategoryItemTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardCategoryItemTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 44
    }
    
    @IBOutlet weak var itemButton: UIButton!
    
    weak var delegate: DashboardCategoryItemTableViewCellDelegate?
    var category_item: CategoryItem? {
        didSet { self.itemButton.setTitle(self.category_item?.name, for: .normal) }
    }
    var isCategoryItemSelected: Bool = false {
        didSet {
            self.itemButton.isSelected = self.isCategoryItemSelected
            self.itemButton.imageView?.tintColor = self.isCategoryItemSelected ? Resources.lightGreen : UIColor.darkGray
        }
    }
    
    // awake
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // images
        self.itemButton.setImage(UIImage(named: "check_off")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.itemButton.setImage(UIImage(named: "check_on")?.withRenderingMode(.alwaysTemplate), for: .selected)
        self.itemButton.tintColor = UIColor.clear
    }
    
    @IBAction func itemClick(_ sender: UIButton) {
        guard let category_item = self.category_item else { return }
        
        // delegate
        self.delegate?.dashboardCategoryItemTableViewCellSelected(cell: self, category_item: category_item)
    }
}
