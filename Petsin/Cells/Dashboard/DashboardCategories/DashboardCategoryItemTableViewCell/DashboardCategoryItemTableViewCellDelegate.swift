//
//  DashboardCategoryItemTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardCategoryItemTableViewCellDelegate: class {
    func dashboardCategoryItemTableViewCellSelected(cell: DashboardCategoryItemTableViewCell, category_item: CategoryItem)
}
