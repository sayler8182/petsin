//
//  DashboardLocationTableViewCell.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import CoreLocation

class DashboardLocationTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardLocationTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 94
    }
    
    @IBOutlet weak var locationTextField: UITextField! {
        didSet { self.locationTextField.delegate = self }
    }
    @IBOutlet weak var gpsButton: UIButton!
    
    weak var delegate: DashboardLocationTableViewCellDelegate?
    var city: City? = nil {
        didSet { self.locationTextField.text = self.city?.name }
    }
    var user_location: CLLocationCoordinate2D? = nil {
        didSet {
            guard user_location != nil else { return }
            self.locationTextField.text = "Moja lokalizacja"
        }
    }
    
    @IBAction func gpsClick(_ sender: UIButton) {
        
        // delegate
        self.delegate?.dashboardLocationTableViewCellGps(cell: self)
    }
    
    @IBAction func locationValueChanged(_ sender: UITextField) {
        let text = sender.text
        
        // delegate
        self.delegate?.dashboardLocationTableViewCellValueChanged(cell: self, location: text)
    }
}

// MARK: UITextFieldDelegate
extension DashboardLocationTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.dashboardLocationTableViewCellEndEditing(cell: self)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.dashboardLocationTableViewCellBeginEditing(cell: self)
    }
}
