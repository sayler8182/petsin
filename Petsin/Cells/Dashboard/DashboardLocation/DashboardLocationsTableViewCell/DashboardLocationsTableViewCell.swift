//
//  DashboardLocationsTableViewCell.swift
//  Petsin
//
//  Created by Mac on 16.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardLocationsTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardLocationsTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 44
    }
    
    @IBOutlet var nameLabel: UILabel!
    
    var city: City? = nil {
        didSet {
            self.nameLabel.text = self.city?.name
        }
    }
}
