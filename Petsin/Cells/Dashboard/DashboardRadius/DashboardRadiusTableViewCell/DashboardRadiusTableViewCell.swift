//
//  DashboardRadiusTableViewCell.swift
//  Petsin
//
//  Created by Mac on 13.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardRadiusTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardRadiusTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 94
    }
    
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    
    weak var delegate: DashboardRadiusTableViewCellDelegate?
    var radius: Int = 50 {
        didSet { self.radiusLabel.text = "\(self.radius) km"}
    }
    
    @IBAction func radiusSliderChanged(_ slider: UISlider) {
        self.radius = Int(round(slider.value))
        
        // delegate
        self.delegate?.dashboardRadiusTableViewCell(cell: self, radius: radius)
    }
} 

