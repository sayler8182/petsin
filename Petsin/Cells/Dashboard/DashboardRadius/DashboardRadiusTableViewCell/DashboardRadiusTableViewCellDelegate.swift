//
//  DashboardRadiusTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 13.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardRadiusTableViewCellDelegate: class {
    func dashboardRadiusTableViewCell(cell: DashboardRadiusTableViewCell, radius: Int)
}
