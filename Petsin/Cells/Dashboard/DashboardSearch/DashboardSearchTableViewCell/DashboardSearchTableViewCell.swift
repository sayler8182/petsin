//
//  DashboardSearchTableViewCell.swift
//  Petsin_PROD
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardSearchTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardSearchTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 70
    }
    
    @IBOutlet weak var searchButton: UIButton!
    
    weak var delegate: DashboardSearchTableViewCellDelegate?
    
    @IBAction func searchClick(_ sender: UIButton) {
        
        // delegate
        self.delegate?.dashboardSearchTableViewCellSearch(cell: self)
    }
}
