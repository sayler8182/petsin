//
//  DashboardSearchTableViewCellDelegate.swift
//  Petsin_PROD
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardSearchTableViewCellDelegate: class {
    func dashboardSearchTableViewCellSearch(cell: DashboardSearchTableViewCell)
}
