//
//  DashboardTypeCollectionViewCell.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Kingfisher

class DashboardTypeCollectionViewCell: UICollectionViewCellBase {
    override class var reuseIdentifier: String { return "DashboardTypeCollectionViewCell" }
    
    override class func getSize(collectionSize: CGSize) -> CGSize {
        let size: CGFloat = 140
        return CGSize(width: size, height: size)
    }
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    fileprivate weak var selectionLayer: CALayer? = nil
    
    var type: Type? {
        didSet {
            self.titleLabel.text = self.type?.name
            self.iconImage.kf.setImage(with: self.type?.image_src, options: [.transition(.fade(0.5))])
        }
    }
    var isTypeSelected: Bool = false {
        didSet { self.setNeedsDisplay() }
    }
    
    // awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // icon image indicator
        self.iconImage.kf.indicatorType = .activity
    }
    
    // draw
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.layer.shadow(radius: 3, opacity: 0.2)
        
        // update selection
        self.updateSelection()
    }
    
    // end displaying
    override func endDisplaying() {
        super.endDisplaying()
        
        // cancel download image
        self.iconImage.kf.cancelDownloadTask()
        self.iconImage.image = nil
    }
    
    // update selection
    fileprivate func updateSelection() {
        self.selectionLayer?.removeFromSuperlayer()
        guard self.isTypeSelected == true else { return }
        let cellSize: CGFloat = self.frame.size.height
        let size: CGFloat = 40
        let layer = CAShapeLayer()
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: cellSize - size, y:0))
        path.addLine(to: CGPoint(x: cellSize, y: 0))
        path.addLine(to: CGPoint(x: cellSize, y: size))
        path.addLine(to: CGPoint(x: cellSize - size, y: 0))
        path.close()
        
        layer.path = path.cgPath
        layer.fillColor = Resources.lightGreen.cgColor
        self.layer.addSublayer(layer)
        self.selectionLayer = layer
    }
}


