//
//  DashboardTypesTableViewCell.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardTypesTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "DashboardTypesTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 345
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            DashboardTypeCollectionViewCell.register(collectionView: self.collectionView)
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    weak var delegate: DashboardTypesTableViewCellDelegate?
    var types: [Type] = [] {
        didSet { self.collectionView.reloadData() }
    }
    var types_selected: [Type] = [] {
        didSet { self.collectionView.reloadData() }
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
extension DashboardTypesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // cell count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.types.count
    }
    
    // cell click
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        let type = self.types[indexPath.row]
        
        // delegate
        self.delegate?.dashboardTypesTableViewCellSelected(cell: self, type: type)
    }
    
    // cell view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: DashboardTypeCollectionViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let type = self.types[indexPath.row]
        let cell = cell as! DashboardTypeCollectionViewCell
        cell.type = type
        cell.isTypeSelected = self.types_selected.contains(where: { $0.id == type.id })
    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return DashboardTypeCollectionViewCell.getSize(collectionSize: collectionView.frame.size)
    }
}

