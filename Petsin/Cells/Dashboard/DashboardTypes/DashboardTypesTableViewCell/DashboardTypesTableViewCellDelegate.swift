//
//  DashboardTypesTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardTypesTableViewCellDelegate: class {
    func dashboardTypesTableViewCellSelected(cell: DashboardTypesTableViewCell, type: Type)
}
