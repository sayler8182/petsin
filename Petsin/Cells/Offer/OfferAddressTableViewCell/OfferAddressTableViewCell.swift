//
//  OfferAddressTableViewCell.swift
//  Petsin
//
//  Created by Mac on 19.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import MapKit

class OfferAddressTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "OfferAddressTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 200
    }
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapLabel: UILabel! {
        didSet { self.mapLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mapTapped(_:)))) }
    }
    @IBOutlet weak var addressView: UIView! {
        didSet { self.addressView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mapTapped(_:)))) }
    }
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneView: UIView! {
        didSet { self.phoneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(phoneTapped(_:)))) }
    }
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailView: UIView! {
         didSet { self.emailView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(emailTapped(_:)))) }
    }
    @IBOutlet weak var emailLabel: UILabel!
    
    weak var delegate: OfferAddressTableViewCellDelegate?
    var offer: Offer?  {
        didSet {
            self.addressLabel.text = self.offer?.full_address
            self.phoneLabel.text = self.offer?.phone
            self.emailLabel.text = self.offer?.email
            self.loadMap()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.mapView.layer.shadow(radius: 3, opacity: 0.2)
    }
    
    @objc func phoneTapped(_ recognizer: UITapGestureRecognizer) {
        guard let phone = self.offer?.phone else { return }
        
        // delegate
        self.delegate?.offerAddressTableViewCellPhone(cell: self, phone: phone)
    }
    
    @objc func emailTapped(_ recognizer: UITapGestureRecognizer) {
        guard let email = self.offer?.email else { return }
        
        // delegate
        self.delegate?.offerAddressTableViewCellEmail(cell: self, email: email)
    }
    
    @objc func mapTapped(_ recognizer: UITapGestureRecognizer) {
        guard let coordinate = self.offer?.coordinate else { return }
        
        // delegate
        self.delegate?.offerAddressTableViewCellMap(cell: self, coordinate: coordinate)
    }
    
    // load map
    fileprivate func loadMap() {
        guard let coordinate = self.offer?.coordinate  else { return }
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(
            coordinate,
            regionRadius * 2.0,
            regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: false)
    }
}
