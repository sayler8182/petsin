//
//  OfferAddressTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 19.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreLocation

protocol OfferAddressTableViewCellDelegate: class {
    func offerAddressTableViewCellPhone(cell: OfferAddressTableViewCell, phone: String)
    func offerAddressTableViewCellEmail(cell: OfferAddressTableViewCell, email: String)
    func offerAddressTableViewCellMap(cell: OfferAddressTableViewCell, coordinate: CLLocationCoordinate2D)
}
