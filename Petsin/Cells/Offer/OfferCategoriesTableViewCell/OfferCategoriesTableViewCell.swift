//
//  OfferCategoriesTableViewCell.swift
//  Petsin
//
//  Created by Mac on 18.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class OfferCategoriesTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "OfferCategoriesTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 60
    }
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            OfferCategoryCollectionViewCell.register(collectionView: self.collectionView)
            self.collectionView.contentInset = UIEdgeInsetsMake(0, 8, 0, 8)
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    
    weak var delegate: OfferCategoriesTableViewCellDelegate?
    var offer: Offer?  {
        didSet { self.items = self.offer?.pets ?? [] }
    }
    fileprivate var items: [OfferCategory] = [] {
        didSet { self.collectionView.reloadData() }
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension OfferCategoriesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // cell count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // cell view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: OfferCategoryCollectionViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! OfferCategoryCollectionViewCell
        cell.item = self.items[indexPath.row]
    }
    
    // end displatying
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! UICollectionViewCellBase
        cell.endDisplaying()
    }
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return OfferCategoryCollectionViewCell.getSize(collectionSize: collectionView.frame.size)
    }
}

