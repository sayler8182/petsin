//
//  OfferContactTableViewCell.swift
//  Petsin
//
//  Created by Mac on 17.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Kingfisher
import AlamofireImage

class OfferContactTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "OfferContactTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 60
    }
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton! 
    
    weak var delegate: OfferContactTableViewCellDelegate?
    var offer: Offer?  {
        didSet {
            self.nameLabel.text = self.offer?.user_full_name
            
            // image
            let radius = self.logoView.bounds.height / 2
            let radiousProcessor = RoundCornerImageProcessor(cornerRadius: radius)
            let placeholder = UIImage(named: "profile")?.af_imageRounded(withCornerRadius: radius)
            self.logoView.kf.setImage(with: self.offer?.user_logo_src, placeholder: placeholder, options: [.processor(radiousProcessor), .transition(.fade(0.5))])
        }
    }
    
    // awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // icon image indicator
        self.logoView.kf.indicatorType = .activity
    }
    
    // end displaying
    override func endDisplaying() {
        super.endDisplaying()
        
        // cancel download image
        self.logoView.kf.cancelDownloadTask()
        self.logoView.image = nil
    }
    
    @IBAction func phoneClick(_ sender: UIButton) {
        guard let phone = self.offer?.phone else { return }
        
        // delegate
        self.delegate?.offerContactTableViewCellPhone(cell: self, phone: phone)
    }
    
    @IBAction func emailClick(_ sender: UIButton) {
        guard let email = self.offer?.email else { return }
        
        // delegate
        self.delegate?.offerContactTableViewCellEmail(cell: self, email: email)
    }
}
