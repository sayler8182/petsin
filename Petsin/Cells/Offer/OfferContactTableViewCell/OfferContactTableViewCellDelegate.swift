//
//  OfferContactTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 17.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol OfferContactTableViewCellDelegate: class {
    func offerContactTableViewCellPhone(cell: OfferContactTableViewCell, phone: String)
    func offerContactTableViewCellEmail(cell: OfferContactTableViewCell, email: String)
}
