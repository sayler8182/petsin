//
//  OfferDetailsTableViewCell.swift
//  Petsin
//
//  Created by Mac on 19.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class OfferDetailsTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "OfferDetailsTableViewCell" }
    
    class func getHeight(tableSize: CGSize, details: OfferDetails) -> CGFloat {
        if details.is_selected == true {
            let font = UIFont.systemFont(ofSize: 16)
            let width = tableSize.width - 32
            let height = details.details.height(width: width, font: font) + 64
            return max(64, ceil(height))
        } else {
            return 110
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var details: OfferDetails?  {
        didSet {
            self.titleLabel.text = self.details?.title
            self.detailsLabel.text = self.details?.details
            self.moreButton.superview?.isHidden = self.details?.is_selected != false
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.moreButton.layer.shadow(color: UIColor.white, radius: 8, opacity: 1)
    }
}

