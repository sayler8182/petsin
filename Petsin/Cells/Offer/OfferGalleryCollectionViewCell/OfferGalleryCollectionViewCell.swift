//
//  OfferGalleryCollectionViewCell.swift
//  Petsin
//
//  Created by Mac on 17.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Kingfisher

class OfferGalleryCollectionViewCell: UICollectionViewCellBase {
    override class var reuseIdentifier: String { return "OfferGalleryCollectionViewCell" }
    
    override class func getSize(collectionSize: CGSize) -> CGSize {
        return collectionSize
    }
    
    @IBOutlet weak var itemImage: UIImageView!
    
    var item: URL? {
        didSet {
            self.itemImage.kf.setImage(with: self.item, options: [.transition(.fade(0.5))])
        }
    }
    var imageToShare: UIImage? {
        return self.itemImage?.image
    }
    
    // awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // item image indicator
        self.itemImage.kf.indicatorType = .activity
    }
    
    // end displaying
    override func endDisplaying() {
        super.endDisplaying()
        
        // cancel download image
        self.itemImage.kf.cancelDownloadTask()
        self.itemImage.image = nil
    }
} 
