//
//  OffersTableViewCell.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Kingfisher

class OffersTableViewCell: UITableViewCellBase {
    override class var reuseIdentifier: String { return "OffersTableViewCell" }
    
    override class func getHeight(tableSize: CGSize) -> CGFloat {
        return 140
    }
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    weak var delegate: OffersTableViewCellDelegate?
    var offer: Offer?  {
        didSet {
            self.nameLabel.text = self.offer?.name
            self.descriptionLabel.text = self.offer?._description
            self.addressLabel.text = self.offer?.address
            self.logoView.kf.setImage(with: self.offer?.logo_src, options: [.transition(.fade(0.5))])
            
            // price
            let price = self.offer?.price_from ?? 0
            self.priceLabel.text = String(format: "%2d.00zł", price)
            
            // is promo
            self.shadowColor = self.offer?.is_promotion == true ? Resources.lightGreen : UIColor.black.withAlphaComponent(0.5)
            let border = self.offer?.is_promotion == true ? Resources.lightGreen : UIColor.clear
            self.shadowView.layer.borderColor = border.cgColor
        }
    }
    var isOfferFavourite: Bool = false {
        didSet {
            self.favouriteButton.isSelected = self.isOfferFavourite
            self.favouriteButton.imageView?.tintColor = self.isOfferFavourite ? Resources.lightGreen : UIColor.darkGray
        }
    }
    var shadowColor: UIColor = UIColor.black {
        didSet { self.setNeedsDisplay() }
    }
    
    // awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // favourites
        self.favouriteButton.setImage(UIImage(named: "favourite")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.favouriteButton.tintColor = UIColor.clear
        
        // icon image indicator
        self.logoView.kf.indicatorType = .activity
        
        // border
        self.shadowView.layer.borderWidth = 1
    }
    
    // draw
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.shadowView.layer.shadow(color: self.shadowColor, radius: 3, opacity: 0.5)
    }
    
    // end displaying
    override func endDisplaying() {
        super.endDisplaying()
        
        // cancel download image
        self.logoView.kf.cancelDownloadTask()
        self.logoView.image = nil
    }
    
    @IBAction func favouriteClick(_ sender: UIButton) {
        guard let offer = self.offer else { return }
        
        // delegate
        self.delegate?.offersTableViewCellFavourite(cell: self, offer_id: offer.id)
    }
}
