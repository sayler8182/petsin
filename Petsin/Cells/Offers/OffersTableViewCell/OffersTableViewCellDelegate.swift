//
//  OffersTableViewCellDelegate.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol OffersTableViewCellDelegate: class {
    func offersTableViewCellFavourite(cell: OffersTableViewCell, offer_id: Int)
}
