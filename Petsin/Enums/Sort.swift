//
//  Sort.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

enum Sort {
    case priceAsc
    case priceDesc
    case nameAsc
    case nameDesc
    
    static var `default`: Sort {
        return .nameAsc
    }
}
