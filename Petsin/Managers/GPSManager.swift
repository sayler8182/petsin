//
//  GPSManager.swift
//  Petsin
//
//  Created by Mac on 13.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreLocation

class GPSManager: NSObject {
    fileprivate let locationManager = CLLocationManager()
    fileprivate var locationChanged: ((_ location: CLLocationCoordinate2D?) -> Void)? = nil
    
    // start location
    func startLoacation(locationChanged: @escaping (_ location: CLLocationCoordinate2D?) -> Void) {
        self.locationChanged = locationChanged
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
    }
    
    // stop location
    func stopLocation() {
        self.locationManager.stopUpdatingLocation()
    }
}

// MARK: CLLocationManagerDelegate
extension GPSManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.locationServicesEnabled() == false {
            self.locationManager.stopUpdatingLocation()
            self.locationChanged?(nil)
        } else {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        let location = manager.location?.coordinate
        self.locationChanged?(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationManager.stopUpdatingLocation()
        self.locationChanged?(nil)
    }
}
