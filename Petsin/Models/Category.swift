//
//  Category.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON

class Category {
    let name: String
    let id: Int
    let items: [CategoryItem]
    
    init(name: String, id: Int, items: [CategoryItem]) {
        self.name = name
        self.id = id
        self.items = items
    }
    
    convenience init?(json: JSON) {
        
        // name
        let name = json["name"].string
        
        // id
        let id = json["id"].int
        
        // items
        var items: [CategoryItem] = []
        json["children"].array?.forEach {
            if let item = CategoryItem(json: $0) {
                items.append(item)
            }
        }
        
        if let name = name,
            let id = id {
            self.init(name: name, id: id, items: items)
        } else {
            return nil
        }
    }
}
