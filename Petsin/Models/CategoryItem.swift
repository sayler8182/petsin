//
//  CategoryItem.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON

class CategoryItem {
    let name: String
    let id: Int
    
    init(name: String, id: Int) {
        self.name = name
        self.id = id
    }
    
    convenience init?(json: JSON) {
        let name = json["name"].string
        let id = json["id"].int
        
        if let name = name,
            let id = id {
            self.init(name: name, id: id)
        } else {
            return nil
        }
    }
}
