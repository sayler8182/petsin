//
//  City.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON
import CoreLocation

class City {
    let name: String
    let id: Int
    let lat: Double
    let lng: Double
    
    lazy var coordinate: CLLocationCoordinate2D = {
        return CLLocationCoordinate2D(latitude: self.lat, longitude: self.lng)
    }()
    
    var dict: [String: Any] {
        return [
            "name": self.name,
            "id": self.id,
            "lat": self.lat,
            "lng": self.lng
        ]
    }
    
    init(name: String, id: Int, lat: Double, lng: Double) {
        self.name = name
        self.id = id
        self.lat = lat
        self.lng = lng
    }
    
    convenience init?(json: JSON) {
        let name = json["name"].string
        let id = json["id"].int
        let lat = json["lat"].double
        let lng = json["lng"].double
        
        if let name = name,
            let id = id,
            let lat = lat,
            let lng = lng {
            self.init(name: name, id: id, lat: lat, lng: lng)
        } else {
            return nil
        }
    }
}
