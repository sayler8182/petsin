//
//  Offer.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON
import CoreLocation

class Offer: NSObject {
    var type: String
    var add_date: Date?
    var subscription: Date?
    var promote: Date?
    var name: String
    var _description: String
    var additional_information: String
    var policy: String
    var courses: String
    var city: String
    var address: String
    var address2: String
    var post_code: String
    var phone: String
    var email: String
    var lat: Double?
    var lng: Double?
    var status: Int
    var price_from: Int?
    var price_details: String
    var logo: String?
    var id: Int
    var is_promotion: Bool
    
    var user_name: String?
    var user_last_name: String?
    var user_logo: String?
    var url_share: String?
    var images: [String]?
    var facilities: [String]?
    var pets: [OfferCategory]?
    
    lazy var logo_src: URL? = {
        guard let logo = self.logo else { return nil }
        return URL(string: logo)
    }()
    lazy var user_logo_src: URL? = {
        guard let user_logo = self.user_logo else { return nil }
        return URL(string: user_logo)
    }()
    lazy var url_share_src: URL? = {
        guard let url_share = self.url_share else { return nil }
        return URL(string: url_share)
    }()
    lazy var images_src: [URL]? = {
        guard let images = self.images else { return nil }
        return images.flatMap { URL(string: $0) }
    }()
    lazy var details_items: [OfferDetails] = {
        var items: [OfferDetails] = []
        if self.facilities_string.isEmpty == false {
            items.append(OfferDetails(title: "UDOGODNIENIA", details: self.facilities_string))
        }
        if self._description.isEmpty == false {
            items.append(OfferDetails(title: "OPIS", details: self._description))
        }
        if self.additional_information.isEmpty == false {
            items.append(OfferDetails(title: "DODATKOWE INFORMACJE", details: self.additional_information))
        }
        if self.policy.isEmpty == false {
            items.append(OfferDetails(title: "POLITYKA WZGLĘDEM ZWIERZĄT", details: self.policy))
        }
        if self.courses.isEmpty == false {
            items.append(OfferDetails(title: "PRZEBIEG", details: self.courses))
        }
        return items
    }()
    
    var coordinate: CLLocationCoordinate2D? {
        guard let lat = self.lat,
            let lng = self.lng else { return nil }
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    var shareText: String {
        return [self.name, self.address]
            .join(separator: "\n")
    }
    var user_full_name: String? {
        return [self.user_name, self.user_last_name]
            .filter { $0?.isEmpty == false }
            .flatMap { $0 }
            .join(separator: " ")
    }
    var full_address: String? {
        return [self.address, self.address2, self.city]
            .filter { $0?.isEmpty == false }
            .flatMap { $0 }
            .join(separator: "\n")
    }
    var facilities_string: String {
        let facilities: [String?] = self.facilities ?? []
        return facilities.join(separator: "\n")
    }
    
    var dict: [String: Any?] {
        var dict: [String: Any?] = [:]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        dict["type"] = type
        if let add_date = add_date {
            dict["addDate"] = ["date": dateFormatter.string(from: add_date) ]
        }
        if let subscription = subscription {
            dict["subscription"] = ["date": dateFormatter.string(from: subscription) ]
        }
        if let promote = promote {
            dict["promote"] =  ["date": dateFormatter.string(from:promote) ]
        }
        dict["name"] = name
        dict["description"] = _description
        dict["additionalInformation"] = additional_information
        dict["policy"] = policy
        dict["courses"] = courses
        dict["city"] = city
        dict["address"] = address
        dict["address2"] = address2
        dict["postCode"] = post_code
        dict["phone"] = phone
        dict["email"] = email
        dict["lat"] = lat
        dict["lng"] = lng
        dict["status"] = status
        dict["priceFrom"] = price_from
        dict["priceDetails"] = price_details
        dict["logo"] = logo
        dict["id"] = id
        dict["is_promotion"] = is_promotion
        return dict
    }
    
    init(type: String, add_date: Date?, subscription: Date?, promote: Date?, name: String, _description: String, additional_information: String, policy: String, courses: String, city: String, address: String, address2: String, post_code: String, phone: String, email: String, lat: Double?, lng: Double?, status: Int, price_from: Int?, price_details: String, logo: String?, id: Int, is_promotion: Bool, user_name: String?, user_last_name: String?, user_logo: String?, url_share: String?, images: [String]?, facilities: [String]?, pets: [OfferCategory]?) {
        self.type = type
        self.add_date = add_date
        self.subscription = subscription
        self.promote = promote
        self.name = name
        self._description = _description
        self.additional_information = additional_information
        self.policy = policy
        self.courses = courses
        self.city = city
        self.address = address
        self.address2 = address2
        self.post_code = post_code
        self.phone = phone
        self.email = email
        self.lat = lat
        self.lng = lng
        self.status = status
        self.price_from = price_from
        self.price_details = price_details
        self.logo = logo
        self.id = id
        self.is_promotion = is_promotion
        
        self.user_name = user_name
        self.user_last_name = user_last_name
        self.user_logo = user_logo
        self.url_share = url_share
        self.images = images
        self.facilities = facilities
        self.pets = pets
    }
    
    convenience init?(json: JSON) {
        let type = json["type"].string ?? ""
        let add_date = json["addDate"]["date"].string ?? ""
        let subscription = json["subscription"]["date"].string ?? ""
        let promote = json["promote"]["date"].string ?? ""
        let name = json["name"].string ?? ""
        let _description = json["description"].string ?? ""
        let additional_information = json["additionalInformation"].string ?? ""
        let policy = json["policy"].string ?? ""
        let courses = json["courses"].string ?? ""
        let city = json["city"].string ?? ""
        let address = json["address"].string ?? ""
        let address2 = json["address2"].string ?? ""
        let post_code = json["postCode"].string ?? ""
        let phone = json["phone"].string ?? ""
        let email = json["email"].string ?? ""
        let lat = json["lat"].double
        let lng = json["lng"].double
        let status = json["status"].int
        let price_from = json["priceFrom"].int
        let price_details = json["priceDetails"].string ?? ""
        let logo = json["logo"].string
        let id = json["id"].int
        let is_promotion = json["is_promotion"].bool ?? false
        
        let user_name = json["user"]["name"].string
        let user_last_name = json["user"]["lastName"].string
        let user_logo = json["user"]["logo"].string
        let url_share = json["url-share"].string
        let images = json["image"].array?.flatMap { $0.string }
        let facilities = json["facilities"].array?.flatMap { $0["name"].string }
        let pets = json["pets"].array?.flatMap { OfferCategory(json: $0) }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let add_date_date = dateFormatter.date(from: add_date)
        let subscription_date = dateFormatter.date(from: subscription)
        let promote_date = dateFormatter.date(from: promote)
        
        if let status = status,
            let id = id {
            self.init(type: type, add_date: add_date_date, subscription: subscription_date, promote: promote_date, name: name, _description: _description, additional_information: additional_information, policy: policy, courses: courses, city: city, address: address, address2: address2, post_code: post_code, phone: phone, email: email, lat: lat, lng: lng, status: status, price_from: price_from, price_details: price_details, logo: logo, id: id, is_promotion: is_promotion,  user_name: user_name, user_last_name: user_last_name, user_logo: user_logo, url_share: url_share, images: images, facilities: facilities, pets: pets)
        } else {
            return nil
        }
    }
}
