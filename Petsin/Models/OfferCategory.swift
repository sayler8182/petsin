//
//  OfferCategory.swift
//  Petsin
//
//  Created by Mac on 17.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON

class OfferCategory {
    let name: String
    let uid: String
    let path: String
    
    lazy var path_src: URL? = {
        return URL(string: self.path)
    }()
    
    init(name: String, uid: String, path: String) {
        self.name = name
        self.uid = uid
        self.path = path
    }
    
    convenience init?(json: JSON) {
        let name = json["name"].string
        let uid = json["uid"].string
        let path = json["path"].string
        
        if let name = name,
            let uid = uid,
            let path = path {
            self.init(name: name, uid: uid, path: path)
        } else {
            return nil
        }
    }
}

