//
//  OfferDetails.swift
//  Petsin
//
//  Created by Mac on 19.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class OfferDetails {
    var title: String
    var details: String
    var is_selected: Bool
    
    init(title: String, details: String) {
        self.title = title
        self.details = details
        self.is_selected = false
    }
}
