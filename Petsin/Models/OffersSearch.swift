//
//  OffersSearch.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreLocation

struct OffersSearch {
    var categories_ids: [Int]
    var city_id: Int?
    var coordinate: CLLocationCoordinate2D?
    var range: Int
    var types_ids: [Int]
    
    var categories_ids_string: String {
        return "[" +
            self.categories_ids.map { "\($0)"}.join(separator: ",") +
        "]"
    } 
    var types_ids_string: String {
        return "[" +
            self.types_ids.map { "\($0)"}.join(separator: ",") +
        "]"
    }
}
