//
//  Type.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import SwiftyJSON

class Type {
    let name: String
    let id: Int
    let order: Int
    let image: String
    
    lazy var image_src: URL? = {
        return URL(string: self.image)
    }()
    
    init(name: String, id: Int, order: Int, image: String) {
        self.name = name
        self.id = id
        self.order = order
        self.image = image
    }
    
    convenience init?(json: JSON) {
        let name = json["name"].string
        let id = json["id"].int
        let order = json["order"].int
        let image = json["image"].string
        
        if let name = name,
            let id = id,
            let order = order,
            let image = image {
            self.init(name: name, id: id, order: order, image: image)
        } else {
            return nil
        }
    }
}
