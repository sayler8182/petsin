//
//  CategoriesProvider.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class CategoriesProvider {
    fileprivate weak var delegate: CategoriesProviderDelegate?
    
    fileprivate lazy var apiCategories: ApiCategories = ApiCategories()
    fileprivate lazy var storage: Storage = Storage.instance
    fileprivate var tries: Int = 0
    
    init(delegate: CategoriesProviderDelegate) {
        self.delegate = delegate
    }
    
    // categories
    func categories() {
        self.tries = 0
        
        // categories request
        self.categoriesRequest()
    }
}

// MARK: - ApiCategories
extension CategoriesProvider {
    
    // categories request
    fileprivate func categoriesRequest() {
        
        // request
        let _ = self.apiCategories.categoriesRequest(
            success: { (categories) in
                print("categories success (\(categories.count))")
                self.storage.categories = categories
                
                // success
                self.delegate?.categoriesProviderSuccess(categories: categories)
        }, error: { (message, result) in
            self.delegate?.categoriesProviderError(error: .categoriesError, message: message)
        }, failure: {
            if self.tries < 3 {
                self.tries += 1
                self.categoriesRequest()
            } else {
                self.delegate?.categoriesProviderError(error: .categoriesFailure, message: nil)
            }
        })
    }
}
