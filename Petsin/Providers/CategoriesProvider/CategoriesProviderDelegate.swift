//
//  CategoriesProviderDelegate.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

public enum CategoriesProviderError: Int {
    case categoriesError                = 0
    case categoriesFailure              = 1
    
    var isError: Bool {
        return
            self == .categoriesError
    }
    
    var isFailure: Bool {
        return
            self == .categoriesFailure
    }
}

protocol CategoriesProviderDelegate: class {
    func categoriesProviderSuccess(categories: [Category])
    func categoriesProviderError(error: CategoriesProviderError, message: String?)
}
