//
//  LocationsProvider.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class LocationsProvider {
    fileprivate weak var delegate: LocationsProviderDelegate?
    
    fileprivate lazy var apiLocations: ApiLocations = ApiLocations()
    fileprivate lazy var storage: Storage = Storage.instance
    fileprivate var tries: Int = 0
    
    init(delegate: LocationsProviderDelegate) {
        self.delegate = delegate
    }
    
    // cities
    func cities() {
        self.tries = 0
        
        // cities request
        self.citiesRequest()
    }
}

// MARK: - ApiLocations
extension LocationsProvider {
    
    // cities request
    fileprivate func citiesRequest() {
        
        // request
        let _ = self.apiLocations.citiesRequest(
            success: { (cities) in
                print("cities success (\(cities.count))")
                self.storage._cities = cities
                self.storage.cities = cities
                
                // success
                self.delegate?.locationsProviderSuccess(cities: cities)
        }, error: { (message, result) in
            self.delegate?.locationsProviderError(error: .citiesError, message: message)
        }, failure: {
            if self.tries < 3 {
                self.tries += 1
                self.citiesRequest()
            } else {
                self.delegate?.locationsProviderError(error: .citiesFailure, message: nil)
            }
        })
    }
}
