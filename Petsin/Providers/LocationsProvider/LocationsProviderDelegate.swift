//
//  LocationsProviderDelegate.swift
//  Petsin
//
//  Created by Mac on 12.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

public enum LocationsProviderError: Int {
    case citiesError                = 0
    case citiesFailure              = 1
    
    var isError: Bool {
        return
            self == .citiesError
    }
    
    var isFailure: Bool {
        return
            self == .citiesFailure
    }
}

protocol LocationsProviderDelegate: class {
    func locationsProviderSuccess(cities: [City])
    func locationsProviderError(error: LocationsProviderError, message: String?)
}
