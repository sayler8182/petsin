//
//  OffersProvider.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class OffersProvider {
    fileprivate weak var delegate: OffersProviderDelegate?
    
    fileprivate lazy var apiOffers: ApiOffers = ApiOffers()
    fileprivate lazy var storage: Storage = Storage.instance
    fileprivate var tries: Int = 0
    
    init(delegate: OffersProviderDelegate) {
        self.delegate = delegate
    }
    
    // offers
    func offers(offers_search: OffersSearch) {
        self.tries = 0
        
        // offers request
        self.offersRequest(offers_search: offers_search)
    }
    
    // offer
    func offer(offer_id: Int) {
        self.tries = 0
        
        // offer request
        self.offerRequest(offer_id: offer_id)
    }
}

// MARK: - ApiOffers
extension OffersProvider {
    
    // offers request
    fileprivate func offersRequest(offers_search: OffersSearch) {
        
        // request
        let _ = self.apiOffers.offersRequest(
            offers_search: offers_search,
            success: { (offers) in
                print("offers success (\(offers.count))")
                
                // success
                self.delegate?.offersProviderSuccess?(offers: offers)
        }, error: { (message, result) in
            self.delegate?.offersProviderError?(error: .offersError, message: message)
        }, failure: {
            if self.tries < 3 {
                self.tries += 1
                self.offers(offers_search: offers_search)
            } else {
                self.delegate?.offersProviderError?(error: .offersFailure, message: nil)
            }
        })
    }
    
    // offer request
    fileprivate func offerRequest(offer_id: Int) {
        
        // request
        let _ = self.apiOffers.offerRequest(
            offer_id: offer_id,
            success: { (offer) in
                print("offer success (\(offer.id))")
                
                // success
                self.delegate?.offersProviderSuccess?(offer: offer)
        }, error: { (message, result) in
            self.delegate?.offersProviderError?(error: .offerError, message: message)
        }, failure: {
            if self.tries < 3 {
                self.tries += 1
                self.offer(offer_id: offer_id)
            } else {
                self.delegate?.offersProviderError?(error: .offerFailure, message: nil)
            }
        })
    }
}

