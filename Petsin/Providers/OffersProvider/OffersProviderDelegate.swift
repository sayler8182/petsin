//
//  OffersProviderDelegate.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

@objc public enum OffersProviderError: Int {
    case offersError                = 0
    case offersFailure              = 1
    
    case offerError                 = 2
    case offerFailure               = 3
    
    var isError: Bool {
        return
            self == .offersError ||
                self == .offerError
    }
    
    var isFailure: Bool {
        return
            self == .offersFailure ||
                self == .offerFailure
    }
}

@objc protocol OffersProviderDelegate: class {
    @objc optional func offersProviderSuccess(offers: [Offer])
    @objc optional func offersProviderSuccess(offer: Offer)
    @objc optional func offersProviderError(error: OffersProviderError, message: String?)
}
