//
//  TypesProvider.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class TypesProvider {
    fileprivate weak var delegate: TypesProviderDelegate?
    
    fileprivate lazy var apiTypes: ApiTypes = ApiTypes()
    fileprivate lazy var storage: Storage = Storage.instance
    fileprivate var tries: Int = 0
    
    init(delegate: TypesProviderDelegate) {
        self.delegate = delegate
    }
    
    // types
    func types() {
        self.tries = 0
        
        // types request
        self.typesRequest()
    }
}

// MARK: - ApiTypes
extension TypesProvider {
    
    // categories request
    fileprivate func typesRequest() {
        
        // request
        let _ = self.apiTypes.typesRequest(
            success: { (types) in
                print("types success (\(types.count))")
                self.storage.types = types
                
                // success
                self.delegate?.typesProviderSuccess(types: types)
        }, error: { (message, result) in
            self.delegate?.typesProviderError(error: .typesError, message: message)
        }, failure: {
            if self.tries < 3 {
                self.tries += 1
                self.typesRequest()
            } else {
                self.delegate?.typesProviderError(error: .typesFailure, message: nil)
            }
        })
    }
}
