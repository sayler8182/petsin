//
//  TypesProviderDelegate.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

public enum TypesProviderError: Int {
    case typesError                = 0
    case typesFailure              = 1
    
    var isError: Bool {
        return
            self == .typesError
    }
    
    var isFailure: Bool {
        return
            self == .typesFailure
    }
}

protocol TypesProviderDelegate: class {
    func typesProviderSuccess(types: [Type])
    func typesProviderError(error: TypesProviderError, message: String?)
}

