//
//  Api.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class Api {
    public var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter
    }()
    public var baseUrl: String { return Config.baseUrl }
    public var udid: String { return Storage.udid }
    
    public var unexpectedErrorMessage: String {
        return "Wystąpił błąd podczas próby pobierania danych"
    }
    
    public var defaultsHeaders: HTTPHeaders {
        let headers: HTTPHeaders = [:]        
        return headers
    }
    
    public func checkError(json: JSON, status: Int?, error: @escaping ((_ message: String, _ result: String?) -> Void)) -> Bool {
        return false
    }
    
    public func checkFailure(message: Error, error: @escaping ((_ message: String, _ result: String?) -> Void), failure: @escaping (() -> Void), cancelled: (() -> Void)? = nil) {
        let ns_message = message as NSError
        if let err = message as? URLError, err.code == URLError.notConnectedToInternet || err.code == URLError.networkConnectionLost{
            
            // internet connection failure
            DispatchQueue.main.async() {
                failure()
            }
        } else if ns_message.code == -999 {
            
            // cancelled
            DispatchQueue.main.async() {
                cancelled?()
            }
        } else {
            
            // other failures
            print("[API] \(ns_message.description)")
            DispatchQueue.main.async() {
                error(self.unexpectedErrorMessage, nil)
            }
        }
    }
    
    public func checkFailure(message: Error, error: @escaping ((_ message: String) -> Void), failure: (() -> Void), cancelled: (() -> Void)? = nil) {
        
        // other failures
        print("[API] \(message)")
        DispatchQueue.main.async {
            error(self.unexpectedErrorMessage)
        }
    }
}

public extension Api {
    
    public func debug(interface: String, headers: HTTPHeaders, parameters: Parameters) {
        let stringsHeaders = headers.map({ "\($0.key): \($0.value)" })
        let stringHeaders = stringsHeaders.joined(separator: "\t")
        let stringsParameters = parameters.map({ "\($0.key): \($0.value)" })
        let stringParameter = stringsParameters.joined(separator: "\t")
        print("[API] \(self.baseUrl)\(interface)")
        print("[API] \(stringHeaders)")
        print("[API] \(stringParameter)")
        print("")
    }
    
    public func getInterface(interface: String, parameters: Parameters = [:]) -> URLConvertible {
        var interface = "\(self.baseUrl)\(interface)"
        
        // get parameters
        for param in parameters {
            if interface.range(of: param.0) != nil {
                interface = interface.replacingOccurrences(of: param.0, with: "\(param.1)")
            }
        }
        
        return interface
    }
    
    public func getStatus(json: JSON) -> Int? {
        return json["status"].int ?? Int(json["status"].string ?? "")
    }
    
    public func isSuccessStatus(json: JSON) -> Bool {
        let status = self.getStatus(json: json)
        return status == 200 || status == 201 || status == 204
    }
}


