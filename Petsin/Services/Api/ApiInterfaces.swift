//
//  ApiInterfaces.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Alamofire

enum ApiInterfaces: String {
    case categories     = "/category/GetList|GET"
    case cities         = "/province/GetCity|POST"
    case offers         = "/offert/GetList|POST"
    case offer          = "/offert/GetOffertDetails|POST"
    case types          = "/pet/GetPets|POST"
    case provinces      = "/province/GetProvince|POST"
    case location       = "/location|POST"
    
    var name: String {
        let split = self.rawValue.components(separatedBy: "|")
        return split.ifExist(index: 0) ?? ""
    }
    
    var method: HTTPMethod {
        let split = self.rawValue.components(separatedBy: "|")
        let string = split.ifExist(index: 1)?.lowercased() ?? ""
        return HTTPMethod(rawValue: string) ?? HTTPMethod.post
    }
}
