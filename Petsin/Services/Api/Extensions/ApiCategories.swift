//
//  ApiCategories.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Alamofire
import SwiftyJSON

class ApiCategories: Api {
    
    // categories request
    // parameters:
    func categoriesRequest(
        success: @escaping (_ categories: [Category]) -> Void,
        error: @escaping (_ message: String, _ result: String?) -> Void,
        failure: @escaping () -> Void,
        cancelled: (() -> Void)? = nil) -> DataRequest {
        
        let interface: ApiInterfaces = ApiInterfaces.categories
        let headers = self.defaultsHeaders
        var parameters: Parameters = [:]
        parameters["udid"] = self.udid

        self.debug(interface: interface.name, headers: headers, parameters: parameters)
        return Alamofire.request(
            self.getInterface(interface: interface.name),
            method: interface.method,
            parameters: parameters,
            headers: headers)
            .responseJSON { (response) in
                DispatchQueue.global(qos: .default).async {
                    switch response.result {
                        
                    case .success:
                        var isError = false
                        var isSuccess = false
                        
                        // parse json
                        if let jsonAsSting = response.result.value {
                            let json = JSON(jsonAsSting)
                            
                            // check error
                            isError = self.checkError(json: json, status: response.response?.statusCode,  error: error)
                            if isError == false {
                                isSuccess = true
                                
                                // parse data
                                var categories: [Category] = []
                                for item in json.array ?? [] {
                                    if let category = Category(json: item) {
                                        categories.append(category)
                                    }
                                }
                                
                                // success response
                                if isSuccess == true {
                                    DispatchQueue.main.async {
                                        success(categories)
                                    }
                                }
                            }
                        }
                        
                        // check unexpected error
                        if isError == false && isSuccess == false {
                            let result = response.result.value as? String
                            DispatchQueue.main.async {
                                error(self.unexpectedErrorMessage, result)
                            }
                        }
                        
                    case .failure(let message):
                        self.checkFailure(message: message, error: error, failure: failure, cancelled: cancelled)
                    }
                }
        }
    }
}
