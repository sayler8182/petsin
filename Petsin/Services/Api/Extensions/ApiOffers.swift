//
//  ApiOffers.swift
//  Petsin
//
//  Created by Mac on 14.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Alamofire
import SwiftyJSON

class ApiOffers: Api {
    
    // offers request
    // parameters:
    func offersRequest(
        offers_search: OffersSearch,
        success: @escaping (_ offers: [Offer]) -> Void,
        error: @escaping (_ message: String, _ result: String?) -> Void,
        failure: @escaping () -> Void,
        cancelled: (() -> Void)? = nil) -> DataRequest {
        
        let interface: ApiInterfaces = ApiInterfaces.offers
        let headers = self.defaultsHeaders
        var parameters: Parameters = [:]
        parameters["udid"] = self.udid
        parameters["aPet_"] = offers_search.types_ids_string
        parameters["iRange"] = offers_search.range
        parameters["aCategory"] = offers_search.categories_ids_string
        if let coordinate = offers_search.coordinate {
            parameters["fLat"] = coordinate.latitude
            parameters["fLng"] = coordinate.longitude
        }
        if let city_id = offers_search.city_id {
            parameters["iCity"] = city_id
        }
        
        self.debug(interface: interface.name, headers: headers, parameters: parameters)
        return Alamofire.request(
            self.getInterface(interface: interface.name),
            method: interface.method,
            parameters: parameters,
            headers: headers)
            .responseJSON { (response) in
                DispatchQueue.global(qos: .default).async {
                    switch response.result {
                        
                    case .success:
                        var isError = false
                        var isSuccess = false
                        
                        // parse json
                        if let jsonAsSting = response.result.value {
                            let json = JSON(jsonAsSting)
                            
                            // check error
                            isError = self.checkError(json: json, status: response.response?.statusCode,  error: error)
                            if isError == false {
                                isSuccess = true
                                
                                // parse data
                                var offers: [Offer] = []
                                for item in json.array ?? [] {
                                    if let offer = Offer(json: item) {
                                        offers.append(offer)
                                    }
                                }
                                
                                // success response
                                if isSuccess == true {
                                    DispatchQueue.main.async {
                                        success(offers)
                                    }
                                }
                            }
                        }
                        
                        // check unexpected error
                        if isError == false && isSuccess == false {
                            let result = response.result.value as? String
                            DispatchQueue.main.async {
                                error(self.unexpectedErrorMessage, result)
                            }
                        }
                        
                    case .failure(let message):
                        self.checkFailure(message: message, error: error, failure: failure, cancelled: cancelled)
                    }
                }
        }
    }
    
    // offer request
    // parameters:
    func offerRequest(
        offer_id: Int,
        success: @escaping (_ offer: Offer) -> Void,
        error: @escaping (_ message: String, _ result: String?) -> Void,
        failure: @escaping () -> Void,
        cancelled: (() -> Void)? = nil) -> DataRequest {
        
        let interface: ApiInterfaces = ApiInterfaces.offer
        let headers = self.defaultsHeaders
        var parameters: Parameters = [:]
        parameters["udid"] = self.udid
        parameters["iOffert"] = offer_id
        
        self.debug(interface: interface.name, headers: headers, parameters: parameters)
        return Alamofire.request(
            self.getInterface(interface: interface.name),
            method: interface.method,
            parameters: parameters,
            headers: headers)
            .responseJSON { (response) in
                DispatchQueue.global(qos: .default).async {
                    switch response.result {
                        
                    case .success:
                        var isError = false
                        var isSuccess = false
                        
                        // parse json
                        if let jsonAsSting = response.result.value {
                            let json = JSON(jsonAsSting)
                            
                            // check error
                            isError = self.checkError(json: json, status: response.response?.statusCode,  error: error)
                            if isError == false {
                                isSuccess = true
                                
                                // parse data
                                if let offer = Offer(json: json["response"]) {
                                
                                    // success response
                                    if isSuccess == true {
                                        DispatchQueue.main.async {
                                            success(offer)
                                        }
                                    }
                                }
                            }
                        }
                        
                        // check unexpected error
                        if isError == false && isSuccess == false {
                            let result = response.result.value as? String
                            DispatchQueue.main.async {
                                error(self.unexpectedErrorMessage, result)
                            }
                        }
                        
                    case .failure(let message):
                        self.checkFailure(message: message, error: error, failure: failure, cancelled: cancelled)
                    }
                }
        }
    }
}

