//
//  GPSSync.swift
//  Petsin
//
//  Created by Mac on 13.12.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import CoreLocation

class GPSSync {
    static var shared: GPSSync = GPSSync()
    fileprivate var defaults: UserDefaults = UserDefaults.standard
    
    fileprivate var ticks: TimeInterval { return Config.syncLocationTime }
    fileprivate let manager: GPSManager = GPSManager()
    fileprivate let apiLocation: ApiLocation = ApiLocation()
    fileprivate var timer: Timer? = nil
    fileprivate var islocationSended: Bool {
        get { return self.defaults.bool(forKey: StorageKey._LOCATION_SENDED.rawValue) }
        set { self.defaults.set(newValue, forKey: StorageKey._LOCATION_SENDED.rawValue) }
    }
    
    // try send location
    func syncLocation() {
        guard self.islocationSended == false else { return }
        self.manager.startLoacation { [weak self] (coordinate) in
            guard let `self` = self else { return }
            if let coordinate = coordinate {
                self.locationRequest(coordinate: coordinate)
            } else {
                self.locationSyncFailed()
            }
        }
    }
    
    // start
    fileprivate func start() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(
            timeInterval: self.ticks,
            target: self,
            selector: #selector(timerTick(_:)),
            userInfo: nil,
            repeats: true)
    }
    
    // stop
    fileprivate func stop() {
        self.timer?.invalidate()
    }
    
    // timer tict
    @objc func timerTick(_ timer: Timer) {
        self.stop()
        self.syncLocation()
    }
    
    // location sync failed
    fileprivate func locationSyncFailed() {
        print("[GPSSync] failed sync location")
        self.start()
    }
}

// MARK: ApiLocation
extension GPSSync {
    fileprivate func locationRequest(coordinate: CLLocationCoordinate2D) {
        let _ = self.apiLocation.locationRequest(
            coordinate: coordinate,
            success: {
               self.islocationSended = true
        }, error: { (message, result) in
            self.locationSyncFailed()
        }, failure: {
            self.locationSyncFailed()
        })
    }
}
