//
//  Storage.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import SimpleKeychain
import SwiftyJSON

enum StorageKey: String {
    case _UDID                  =   "_UDID"
    case _LOCATION_SENDED       =   "_LOCATION_SENDED"
    case _CITIES                =   "_CITIES"
    case _FAVOURITES_IDS        =   "_FAVOURITES_IDS"
    case _FAVOURITES            =   "_FAVOURITES"
}

class Storage {
    static var instance: Storage = Storage()
    private var defaults: UserDefaults = UserDefaults.standard
    
    // TEMP
    var categories: [Category] = []
    var cities: [City] = []
    var types: [Type] = []
    
    // APP VERSION
    static var appVersion: String {
        get {
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                return version
            } else {
                return "0.0.0"
            }
        }
    }
    
    // _UDID
    static var udid: String {
        get {
            if let tmp = A0SimpleKeychain().string(forKey: StorageKey._UDID.rawValue) {
                return tmp;
            } else {
                let tmp = NSUUID().uuidString
                A0SimpleKeychain().setString(tmp, forKey: StorageKey._UDID.rawValue)
                return tmp;
            }
        }
    }
    
    // _CITIES
    var _cities: [City] {
        get {
            let string = self.defaults.string(forKey: StorageKey._CITIES.rawValue) ?? ""
            let json = JSON(parseJSON: string)
            var cities: [City] = []
            for item in json.array ?? [] {
                if let city = City(json: item) {
                    cities.append(city)
                }
            }
            return cities
        }
        set {
            let dict: [[String: Any]] = newValue.map { $0.dict }
            let json = JSON(dict)
            let string = json.rawString() ?? ""
            self.defaults.set(string, forKey: StorageKey._CITIES.rawValue)
            self.defaults.synchronize()
        }
    }
    
    // _FAVOURITES_IDS
    var favourites_ids: [Int] {
        get {
            let string = self.defaults.string(forKey: StorageKey._FAVOURITES_IDS.rawValue) ?? ""
            return string.components(separatedBy: ";").flatMap { Int($0) }
        }
        set {
            let string = newValue.map { "\($0)" }.join(separator: ";")
            self.defaults.set(string, forKey: StorageKey._FAVOURITES_IDS.rawValue)
            self.defaults.synchronize()
        }
    }
    
    // _FAVOURITES
    var favourites: [Offer] {
        get {
            let string = self.defaults.string(forKey: StorageKey._FAVOURITES.rawValue) ?? ""
            let json = JSON(parseJSON: string)
            var offers: [Offer] = []
            for item in json.array ?? [] {
                if let offer = Offer(json: item) {
                    offers.append(offer)
                }
            }
            return offers
        }
        set {
            let dict: [[String: Any?]] = newValue.map { $0.dict }
            let json = JSON(dict)
            let string = json.rawString() ?? ""
            self.defaults.set(string, forKey: StorageKey._FAVOURITES.rawValue)
            self.defaults.synchronize()
        }
    }
}
