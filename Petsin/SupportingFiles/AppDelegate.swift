//
//  AppDelegate.swift
//  Petsin
//
//  Created by Mac on 08.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD

extension UIApplication {
    var statusBarView: UIView? {
        return self.value(forKey: "statusBar") as? UIView
    }
}

class AppDelegate: UIResponder, UIApplicationDelegate {    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarView?.backgroundColor = Resources.darkGreen
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Progress HUD
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.custom)
        SVProgressHUD.setBackgroundLayerColor(UIColor.black.withAlphaComponent(0.4))
        
        // Location sync
        GPSSync.shared.syncLocation()
        
        return true
    } 
}

