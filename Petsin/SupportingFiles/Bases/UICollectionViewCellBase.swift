//
//  UICollectionViewCellBase.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class UICollectionViewCellBase: UICollectionViewCell {
    class var reuseIdentifier: String { return "" }
    
    // get nib for register
    class func getNib() -> UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    // get cell size
    class func getSize(collectionSize: CGSize) -> CGSize {
        return collectionSize
    }
    
    // register for collection view
    class func register(collectionView: UICollectionView) {
        collectionView.register(self.getNib(), forCellWithReuseIdentifier: self.reuseIdentifier)
    }
    
    //  end displaying
    func endDisplaying() { }
}

