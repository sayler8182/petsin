//
//  UITableViewCellBase.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class UITableViewCellBase: UITableViewCell {
    class var reuseIdentifier: String { return "" }
    
    // get nib for register
    class func getNib() -> UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
    
    // get cell height
    class func getHeight(tableSize: CGSize) -> CGFloat {
        return 44
    }
    
    // register for table view
    class func register(tableView: UITableView) {
        tableView.register(self.getNib(), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    //  end displaying
    func endDisplaying() { }
}
