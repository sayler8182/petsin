//
//  UIViewControllerBase.swift
//  Petsin
//
//  Created by Mac on 08.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import  UIKit

class UIViewControllerBase: UIViewController {
    weak var alert: UIAlertController?
    lazy var headerError: String = "Wystąpił błąd"
    lazy var headerFailure: String = "Wystąpił błąd"
    lazy var detailsFailure: String = "Brak połączenia z Internetem"
    lazy var ok: String = "Ok"
    lazy var retry: String = "Ponów próbę"
    lazy var loadingMessage: String = "Ładowanie..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // auto adjust scroll
        self.automaticallyAdjustsScrollViewInsets = false
        
        // prepare view
        self.prepareView()
    }
    
    deinit {
        
        print("[DEINIT] \(type(of: self))")
    }
    
    // prepare view
    func prepareView() {
        
        // set navigation bar
        self.setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // set satus bar
        self.setStatusBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // swipe to back
        self.navigationController?.setStateForSwipeToBack()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // end editing
        self.view.endEditing(false)
        
        // cancel tasks
        self.cancelTasks()
    }
    
    @IBAction func backClick(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func cancelTasks() { }
    
    // set status bar
    func setStatusBar() {
        
        // show status bar
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setNavigationBar() { }
    
    // show menu
    func showMenu(sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let contactAction = UIAlertAction(title: "Kontakt", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.navigationController?.pushViewController(WebViewController.controller(with: Config.contact_url), animated: true)
        })
        let termsAction = UIAlertAction(title: "Regulamin", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.navigationController?.pushViewController(WebViewController.controller(with: Config.terms_url), animated: true)
        })
        let policyAction = UIAlertAction(title: "Polityka prywatności", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.navigationController?.pushViewController(WebViewController.controller(with: Config.policy_url), animated: true)
        })
        let favouritesAction = UIAlertAction(title: "Ulubione", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            let controller = OffersViewController.controller
            controller.type = .favourites
            controller.offers = Storage.instance.favourites
            self.navigationController?.pushViewController(controller, animated: true)
        })
        let cancelAction = UIAlertAction(title: "Anuluj", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
        })
        
        alertController.addAction(contactAction)
        alertController.addAction(termsAction)
        alertController.addAction(policyAction)
        if (self as? OffersViewController)?.type != .favourites {
            alertController.addAction(favouritesAction)
        }
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}
