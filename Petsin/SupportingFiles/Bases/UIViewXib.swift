//
//  UIViewXib.swift
//  Petsin
//
//  Created by Mac on 16.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import SnapKit

class UIViewXib: UIView {
    var xib: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadFromNib()
    }
    
    func loadFromNib() {
        let xib = Bundle(for: type(of: self)).loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as! UIView
        xib.frame = self.bounds;
        xib.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.insertSubview(xib, at: 0)
        
        // save reference
        self.xib = xib
        
        // prepare view
        self.prepareView()
    }
    
    // to override in subclass
    func prepareView() { }
    
    // hide keyboard
    func hideKeyboard() {
        self.endEditing(false)
    }
    
    // view did layout subviews
    func viewDidLayoutSubviews() { }
    
    // handle content click
    func handleClick() { }
    
    // show view
    func showView(superview: UIView?, animated: Bool) {
        guard let superview = superview else { return }
        self.layer.removeAllAnimations()
        
        if self.superview == nil {
            
            // create first time
            superview.addSubview(self)
            
            // constraint
            self.snp.makeConstraints { (make) in
                make.leading.equalTo(superview)
                make.top.equalTo(superview)
                make.bottom.equalTo(superview)
                make.trailing.equalTo(superview)
            }
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        
        // animation
        if animated {
            self.alpha = 0
            UIView.animate(
                withDuration: 0.7,
                animations: {
                    self.alpha = 1
            })
        } else {
            self.alpha = 1
        }
    }
    
    // dismiss view
    func dismissView(animated: Bool) {
        self.layer.removeAllAnimations()
        
        // animation
        if animated {
            UIView.animate(
                withDuration: 0.7,
                animations: {
                    self.alpha = 0
            }, completion: { isSuccess in
                if isSuccess {
                    self.removeFromSuperview()
                }
            })
        } else {
            self.alpha = 0
            self.removeFromSuperview()
        }
    }
}

