//
//  Queue.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

public class Queue {
    
    var isStarted: Bool = false
    private var last_action: (() -> Void)? = nil
    private var queue: [(() -> Void)] = []
    private var endAction: ((_ isSuccess: Bool, _ message: String?) -> Void)?
    
    public init() { }
    
    public func begin() {
        self.isStarted = true
        self.beginNext()
    }
    
    public func add(action: @escaping (() -> Void)) {
        self.queue.insert(action, at: self.queue.count)
    }
    
    public func beginNext() {
        self.isStarted = true
        if 0 < self.queue.count {
            let action = self.queue[0]
            
            // remove this item
            self.last_action = self.queue.removeFirst()
            
            action()
        } else {
            self.isStarted = false
            self.endAction?(true, nil)
        }
    }
    
    public func error(message: String?) {
        if let last_action = self.last_action {
            self.queue.insert(last_action, at: 0)
        }
        self.endAction?(false, message)
        self.isStarted = false
    }
    
    public func disposeActions() {
        self.queue.removeAll()
        self.last_action = nil
        self.isStarted = false
    }
    
    public func dispose() {
        self.queue.removeAll()
        self.last_action = nil
        self.endAction = nil
        self.isStarted = false
    }
    
    public func setEnd(action: @escaping ((_ isSuccess: Bool, _ message: String?) -> Void)) {
        self.endAction = action
    }
}
