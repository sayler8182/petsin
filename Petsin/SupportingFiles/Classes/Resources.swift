//
//  Resources.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class Resources {
    static var lightGreen: UIColor = UIColor(red: 168.0/255.0, green: 207.0/255.0, blue: 56.0/255.0, alpha: 1)
    static var darkGreen: UIColor = UIColor(red: 117.0/255.0, green: 144/255.0, blue: 39.0/255.0, alpha: 1)
}
