//
//  ConfigProtocol.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

enum ConfigType {
    case prod
    case dev
    case local
}

protocol ConfigProtocol: class {
    static var type: ConfigType { get }
    static var versionFlag: String { get }
    static var baseUrl: String { get }
    static var targetName: String { get }
    static var imageAnimation: TimeInterval { get }
    
    static var contact_url: URL? { get }
    static var terms_url: URL? { get }
    static var policy_url: URL? { get }
}

extension ConfigProtocol {
    static func action(forMany types: [ConfigType], action: (() -> Void)) {
        if types.contains(Config.type) {
            action()
        }
    }
    
    static func action(for type: ConfigType, action: (() -> Void)) {
        if Config.type == type {
            action()
        }
    }
    
    static func action(except type: ConfigType, action: (() -> Void)) {
        if Config.type != type {
            action()
        }
    }
}


