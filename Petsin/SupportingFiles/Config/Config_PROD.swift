//
//  Config_LOCAL.swift
//  Petsin
//
//  Created by Mac on 08.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

class Config: ConfigProtocol {
    static var type: ConfigType = .prod
    static var versionFlag: String = ""
    static var baseUrl: String = "http://petsin-api.steccom.pl/web/app_dev.php"
    static var targetName: String = "PROD"
    static var imageAnimation: TimeInterval = 0.3
    static var syncLocationTime: TimeInterval = 60
    
    static var contact_url: URL?    = URL(string: "http://home.petsin.pl/kontakt")
    static var terms_url: URL?      = URL(string: "http://home.petsin.pl/regulamin")
    static var policy_url: URL?     = URL(string: "http://home.petsin.pl/page/polityka-prywatnosci")
    
    // interfaces
    // http://petsin-api.steccom.pl/web/app_dev.php/api/testInterfces
}
