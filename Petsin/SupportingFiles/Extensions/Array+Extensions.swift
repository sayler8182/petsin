//
//  Array+Extensions.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

public extension Array  {
    public static func join(strings: [String?], separator: String) -> String {
        
        let strings = strings
            .filter { $0 != nil && $0?.isEmpty == false}
            .map { $0! }
        
        let stringData = strings.joined(separator: separator)
        return stringData
    }
    
    public func ifExist(index: Int) -> Element? {
        if index < self.count && index >= 0 {
            return self[index]
        } else {
            return nil
        }
    }
}

public extension Array where Element == String? {
    
    public func join(separator: String = " ") -> String {
        let strings: [String?] = Array(self)
        let stringData = Array<String>.join(strings: strings, separator: separator)
        return stringData
    }
}

