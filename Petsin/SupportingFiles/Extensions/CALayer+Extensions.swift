//
//  CALayer+Extensions.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import AudioToolbox

public extension CALayer {
    
    // remove all shadow
    public func removeShadow(mask: Bool = true) {
        self.shadowColor = UIColor.black.cgColor
        self.shadowOffset = CGSize.zero
        self.shadowRadius = 0
        self.shadowOpacity = 1
        self.masksToBounds = mask
        self.shadowPath = nil
    }
    
    // shadow
    public func shadow(color: UIColor = UIColor.black, offset: CGSize = CGSize.zero, radius: CGFloat = 1, opacity: Float = 0.2) {
        self.shadow(color: color, offset: offset, radius: radius, opacity: opacity, cornerRadius: self.cornerRadius)
    }
    
    // shadow
    public func shadow(color: UIColor = UIColor.black, offset: CGSize = CGSize.zero, radius: CGFloat = 1, opacity: Float = 0.2, cornerRadius: CGFloat? = nil) {
        self.shadowColor = color.cgColor
        self.shadowOffset = offset
        self.shadowRadius = radius
        self.shadowOpacity = opacity
        self.masksToBounds = false
        
        if let cornerRadius = cornerRadius, cornerRadius != 0 {
            self.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            self.shadowPath = UIBezierPath(rect: bounds).cgPath
        }
    }
    
    // rastarize layer
    public func rasterize() {
        self.shouldRasterize = true
        self.rasterizationScale = UIScreen.main.scale
        self.drawsAsynchronously = true
    }
    
    // perform vibration
    public func performVibration(center: CGPoint) {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: center.x - 10, y: center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: center.x + 10, y: center.y))
        self.add(animation, forKey: "position")
    }
}

public extension CAShapeLayer {
    public func drawCircleAtLocation(location: CGPoint, radius: CGFloat, color: UIColor, filled: Bool) {
        self.fillColor = filled ? color.cgColor : UIColor.white.cgColor
        self.strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        self.path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
    
    public func drawEllipseAtLocation(location: CGPoint, sizeX: CGFloat, sizeY: CGFloat, color: UIColor, filled: Bool) {
        self.fillColor = filled ? color.cgColor : UIColor.white.cgColor
        self.strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - sizeX, y: location.y - sizeY)
        let bounds = CGRect(origin: origin, size: CGSize(width: sizeX * 2, height: sizeY * 2))
        let radius = min(sizeX, sizeY)
        self.path = UIBezierPath(roundedRect: bounds, byRoundingCorners:[.topRight, .topLeft, .bottomRight, .bottomLeft], cornerRadii: CGSize(width: radius, height: radius)).cgPath
    }
}

