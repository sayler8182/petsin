//
//  String+Extensions.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

extension String {
    
    // height
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let string = NSAttributedString(
            string: self,
            attributes: [NSAttributedStringKey.font: font])
        let size = string.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        
        return size.height
    }
}
