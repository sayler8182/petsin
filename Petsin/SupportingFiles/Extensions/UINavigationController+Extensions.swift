//
//  UINavigationController+Extensions.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    // back to root
    func backToRoot(animated: Bool = true) {
        self.popToRootViewController(animated: animated)
    }
    
    // clear view controllers stack
    func clearStack(removeLast: Bool = false) {
        
        let controllers = self.viewControllers
        if let last = controllers.last {
            if removeLast == false {
                self.viewControllers = [last]
            } else {
                self.viewControllers = []
            }
        }
        
        // swipe state
        self.navigationController?.setStateForSwipeToBack()
    }
    
    // swipe back
    func setStateForSwipeToBack() {
        let canGoBack = 1 < self.viewControllers.count
        
        self.interactivePopGestureRecognizer?.isEnabled = canGoBack
        self.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
    }
}
