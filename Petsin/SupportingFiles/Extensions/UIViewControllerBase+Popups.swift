//
//  UIViewControllerBase+Popups.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import SVProgressHUD

extension UIViewControllerBase {
    fileprivate var root: UIViewController? {
        return self
    }
}

extension UIViewControllerBase {
    
    // show error
    func showError(
        message: String?,
        isFailure: Bool,
        dismissText: String? = "OK",
        dismiss: (() -> Void)? = nil,
        retry: (() -> Void)? = nil) {
        
        if isFailure == true {
            self.showInternetError(
                dismissText: dismissText ?? "OK",
                dismiss: dismiss,
                retry: retry)
        } else {
            self.showError(
                controller: self.root,
                message: message,
                dismissText: dismissText,
                dismiss: dismiss,
                retry: retry)
        }
    }
    
    // show error
    func showError(
        message: String?,
        dismissText: String? = "OK",
        dismiss: (() -> Void)? = nil,
        retry: (() -> Void)? = nil) {
        
        self.showError(
            controller: self.root,
            message: message,
            dismissText: dismissText,
            dismiss: dismiss,
            retry: retry)
    }
    
    // show error
    func showError(
        controller: UIViewController?,
        message: String?,
        dismissText: String? = "OK",
        dismiss: (() -> Void)?,
        retry: (() -> Void)?) {
        
        self.showMessage(
            controller: controller,
            header: self.headerError,
            message: message,
            primaryLabel: dismissText,
            secondaryLabel: self.retry,
            primaryAction: dismiss,
            secondaryAction: retry)
    }
    
    // internet error
    func showInternetError(
        dismissText: String = "OK",
        dismiss: (() -> Void)? = nil,
        retry: (() -> Void)? = nil) {
        
        self.showInternetError(
            controller: self.root,
            dismissText: dismissText,
            dismiss: dismiss,
            retry: retry)
    }
    
    // internet error
    func showInternetError(
        controller: UIViewController?,
        dismissText: String = "OK",
        dismiss: (() -> Void)? = nil,
        retry: (() -> Void)? = nil) {
        
        self.showError(
            controller: controller,
            message: self.detailsFailure,
            dismissText: dismissText,
            dismiss: dismiss,
            retry: retry)
    }
    
    // show message
    func showMessage(
        header: String?,
        message: String?,
        primaryLabel: String?,
        secondaryLabel: String? = nil,
        primaryAction: (() -> Void)? = nil,
        secondaryAction: (() -> Void)? = nil) {
        
        self.showMessage(
            controller: self.root,
            header:header,
            message: message,
            primaryLabel: primaryLabel,
            secondaryLabel: secondaryLabel,
            primaryAction: primaryAction,
            secondaryAction: secondaryAction)
    }
    
    // show message
    func showMessage(
        controller: UIViewController?,
        header: String?,
        message: String?,
        primaryLabel: String?,
        secondaryLabel: String?,
        primaryAction: (() -> Void)? = nil,
        secondaryAction: (() -> Void)? = nil) {
        
        let alert = UIAlertController(title: header ?? "", message: message ?? "", preferredStyle: .alert);
        
        if let primaryLabel = primaryLabel, let primaryAction = primaryAction {
            alert.addAction(UIAlertAction(title: primaryLabel, style: .default, handler: {
                (action: UIAlertAction) -> Void in
                primaryAction();
            }));
        }
        
        if let secondaryLabel = secondaryLabel, let secondaryAction = secondaryAction {
            alert.addAction(UIAlertAction(title: secondaryLabel, style: .default, handler: {
                (action: UIAlertAction) -> Void in
                secondaryAction()
            }))
        }
        
        controller?.present(alert, animated: true, completion: nil);
        self.alert = alert
    }
    
    // show loader
    func showGlobalLoader() {
        SVProgressHUD.show()
    }
    
    // dismiss loader
    func dismissGlobalLoader() {
        SVProgressHUD.dismiss()
    }
    
    // disable gestures
    func disableGestures() {
        self.navigationController?.view.isUserInteractionEnabled = false
        self.view.isUserInteractionEnabled = false
    }
    
    // enable gestures
    func enableGestures() {
        self.navigationController?.view.isUserInteractionEnabled = true
        self.view.isUserInteractionEnabled = true
    }
    
    // show activity
    func showActivity(activity: UIAlertController, sourceView: UIView? = nil) {
        
        if let popoverPresentationController = activity.popoverPresentationController,
            let sourceView = sourceView {
            popoverPresentationController.sourceView = self.root?.view
            
            let origin = sourceView.convert(sourceView.bounds.origin, to: nil)
            popoverPresentationController.sourceRect = CGRect(x: origin.x, y: origin.y, width: sourceView.frame.width, height: sourceView.frame.height)
        }
        self.root?.present(activity, animated: true, completion: nil)
    }
}

