//
//  DashboardViewController.swift
//  Petsin
//
//  Created by Mac on 08.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import CoreLocation

enum DashboardCellType {
    case types
    case location
    case radius
    case categories
    case category_item
    case search
}

class DashboardViewController: UIViewControllerBase {
    class var controller: DashboardViewController {
        return UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "Dashboard") as! DashboardViewController
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            DashboardTypesTableViewCell.register(tableView: self.tableView)
            DashboardLocationTableViewCell.register(tableView: self.tableView)
            DashboardRadiusTableViewCell.register(tableView: self.tableView)
            DashboardCategoriesTableViewCell.register(tableView: self.tableView)
            DashboardCategoryItemTableViewCell.register(tableView: self.tableView)
            DashboardSearchTableViewCell.register(tableView: self.tableView)
        }
    }
    
    fileprivate var locationsViewExist: Bool = false
    fileprivate lazy var locationsView: DashboardLocationsView = {
        self.locationsViewExist = true
        let locationsView = DashboardLocationsView(frame: CGRect(origin: .zero, size: CGSize(width: 300, height: 300)))
        locationsView.delegate = self
        locationsView.cities = Storage.instance.cities
        return locationsView
    }()
    
    fileprivate lazy var gpsManager: GPSManager = GPSManager()
    fileprivate lazy var offersProvider: OffersProvider = OffersProvider(delegate: self)
    fileprivate var items: [DashboardCellType] = []
    
    fileprivate var categories: [Category]          { return Storage.instance.categories }
    fileprivate var cities: [City]                  { return Storage.instance.cities }
    fileprivate var types: [Type]                   { return Storage.instance.types }
    fileprivate var category_items: [CategoryItem]  = []
    
    fileprivate var user_location: CLLocationCoordinate2D?      = nil
    fileprivate var types_selected: [Type]                      = []
    fileprivate var city_selected: City?                        = nil
    fileprivate var radius_selected: Int                        = 50
    fileprivate var categories_selected: [Category]             = []
    fileprivate var category_items_selected: [CategoryItem]     = []
    
    // prepare view
    override func prepareView() {
        super.prepareView()
        
        // clear stack
        self.navigationController?.clearStack()
        
        // load items
        self.loadItems()
    }
    
    override func setNavigationBar() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
    }
    
    @IBAction func menuClick(_ sender: UIBarButtonItem) {
        self.showMenu(sender: sender)
    }
    
    // load items
    fileprivate func loadItems() {
        var items: [DashboardCellType] = []
        items.append(.types)
        items.append(.location)
        items.append(.radius)
        items.append(.categories)
        items.append(.search)
        self.items = items
        self.tableView.reloadData()
    }
}

// MARK: DashboardTypesTableViewCellDelegate
extension DashboardViewController: DashboardTypesTableViewCellDelegate {
    func dashboardTypesTableViewCellSelected(cell: DashboardTypesTableViewCell, type: Type) {
        
        // unselect
        if let index = self.types_selected.index(where: { $0.id == type.id }) {
            self.types_selected.remove(at: index)
        } else {
            // select
            self.types_selected.append(type)
        }
        cell.types_selected = self.types_selected
    }
}

// MARK: DashboardLocationTableViewCellDelegate
extension DashboardViewController: DashboardLocationTableViewCellDelegate {
    func dashboardLocationTableViewCellGps(cell: DashboardLocationTableViewCell) {
        self.showGlobalLoader()
        self.gpsManager.startLoacation { [weak self] (location) in
            guard let `self` = self else { return }
            self.dismissGlobalLoader()
            self.user_location = location
            cell.user_location = location
            
            if location == nil {
                self.showError(message: "Nie udało się pobrać lokalizacji", dismissText: "OK", dismiss: { })
            } else {
                self.city_selected = nil
            }
        }
    }
    
    func dashboardLocationTableViewCellValueChanged(cell: DashboardLocationTableViewCell, location: String?) {
        let origin = cell.frame.origin
        let superview = cell.superview!
        self.locationsView.query = location
        self.locationsView.showView(superview: superview, origin: origin, animated: true)
    }
    
    func dashboardLocationTableViewCellBeginEditing(cell: DashboardLocationTableViewCell) {
        self.tableView.setContentOffset(CGPoint(x: 0, y: 64), animated: true)
    }
    
    func dashboardLocationTableViewCellEndEditing(cell: DashboardLocationTableViewCell) {
        self.locationsView.dismissView(animated: true)
    }
}

// MARK: DashboardRadiusTableViewCellDelegate
extension DashboardViewController: DashboardRadiusTableViewCellDelegate {
    func dashboardRadiusTableViewCell(cell: DashboardRadiusTableViewCell, radius: Int) {
        self.radius_selected = radius
    }
}
    
// MARK: DashboardCategoriesTableViewCellDelegate
extension DashboardViewController: DashboardCategoriesTableViewCellDelegate {
    func dashboardCategoriesTableViewCellSelected(cell: DashboardCategoriesTableViewCell, category: Category) {
        
        // unselect
        if let index = self.categories_selected.index(where: { $0.id == category.id }) {
            let category = self.categories_selected[index]
            self.categories_selected.remove(at: index)
            
            // remove category items
            category.items.forEach { category_item in
                
                // remove selected category items
                if let index = self.category_items_selected.index(where: { $0.id == category_item.id }) {
                    self.category_items_selected.remove(at: index)
                }
                
                // remove category item
                UIView.performWithoutAnimation {
                    if let index = self.category_items.index(where: { $0.id == category_item.id }) {
                        self.tableView.beginUpdates()
                        self.category_items.remove(at: index)
                        let indexPath = IndexPath(row: index + 4, section: 0)
                        self.items.remove(at: indexPath.row)
                        self.tableView.deleteRows(at: [indexPath], with: .top)
                        self.tableView.endUpdates()
                    }
                }
            }
            
        } else {
            // select
            self.categories_selected.append(category)
            
            // add category items
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                category.items.forEach { (category_item) in
                    self.items.insert(.category_item, at: self.items.count - 1)
                    self.category_items.append(category_item)
                    let indexPath = IndexPath(row: self.items.count - 2, section: 0)
                    self.tableView.insertRows(at: [indexPath], with: .automatic)
                }
                self.tableView.endUpdates()
            }
        }
        cell.categories_selected = self.categories_selected
    }
}


// MARK: DashboardCategoryItemTableViewCellDelegate
extension DashboardViewController: DashboardCategoryItemTableViewCellDelegate {
    func dashboardCategoryItemTableViewCellSelected(cell: DashboardCategoryItemTableViewCell, category_item: CategoryItem) {
        
        // unselect
        if let index = self.category_items_selected.index(where: { $0.id == category_item.id }) {
            self.category_items_selected.remove(at: index)
            cell.isCategoryItemSelected = false
        } else {
            // select
            self.category_items_selected.append(category_item)
            cell.isCategoryItemSelected = true
        }
    }
}

// MARK: DashboardSearchTableViewCellDelegate
extension DashboardViewController: DashboardSearchTableViewCellDelegate {
    func dashboardSearchTableViewCellSearch(cell: DashboardSearchTableViewCell) {
        
        // prepare request
        let offers_search = OffersSearch(
            categories_ids: self.category_items_selected.map { $0.id },
            city_id: self.city_selected?.id,
            coordinate: self.user_location,
            range: self.radius_selected,
            types_ids: self.types_selected.map { $0.id })
        self.showGlobalLoader()
        self.offersProvider.offers(offers_search: offers_search)
    }
}

// MARK: DashboardLocationsViewDelegate
extension DashboardViewController: DashboardLocationsViewDelegate {
    func dashboardLocationsViewSelected(city: City) {
        self.city_selected = city
        self.user_location = nil
        self.locationsView.dismissView(animated: true)
        
        // refresh location
        if let index = self.items.index(where: { $0 == .location }) {
            let indexPath = IndexPath(row: index, section: 0)
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
        }
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    // cell count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // cell view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.row]
        switch item {
        case .types:
            return tableView.dequeueReusableCell(withIdentifier: DashboardTypesTableViewCell.reuseIdentifier, for: indexPath)
        case .location:
            return tableView.dequeueReusableCell(withIdentifier: DashboardLocationTableViewCell.reuseIdentifier, for: indexPath)
        case .radius:
            return tableView.dequeueReusableCell(withIdentifier: DashboardRadiusTableViewCell.reuseIdentifier, for: indexPath)
        case .categories:
            return tableView.dequeueReusableCell(withIdentifier: DashboardCategoriesTableViewCell.reuseIdentifier, for: indexPath)
        case .category_item:
            return tableView.dequeueReusableCell(withIdentifier: DashboardCategoryItemTableViewCell.reuseIdentifier, for: indexPath)
        case .search:
            return tableView.dequeueReusableCell(withIdentifier: DashboardSearchTableViewCell.reuseIdentifier, for: indexPath)
        }
    }
    
    // cell will appear
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        switch item {
        case .types:
            let cell = cell as! DashboardTypesTableViewCell
            cell.delegate = self
            cell.types = self.types
            cell.types_selected = self.types_selected
        case .location:
            let cell = cell as! DashboardLocationTableViewCell
            cell.delegate = self
            cell.user_location = self.user_location
            cell.city = self.city_selected
        case .radius:
            let cell = cell as! DashboardRadiusTableViewCell
            cell.delegate = self
            cell.radius = self.radius_selected
        case .categories:
            let cell = cell as! DashboardCategoriesTableViewCell
            cell.delegate = self
            cell.categories = self.categories
        case .category_item:
            let index = indexPath.row - 4
            let category_item = self.category_items[index]
            let cell = cell as! DashboardCategoryItemTableViewCell
            cell.delegate = self
            cell.category_item = category_item
            cell.isCategoryItemSelected = self.category_items_selected.contains(where: { $0.id == category_item.id })
        case .search:
            let cell = cell as! DashboardSearchTableViewCell
            cell.delegate = self
        }
    }
    
    // cell end displaying
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! UITableViewCellBase
        cell.endDisplaying()
    }
    
    // cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.items[indexPath.row]
        switch item {
        case .types:
            return DashboardTypesTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .location:
            return DashboardLocationTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .radius:
            return DashboardRadiusTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .categories:
            return DashboardCategoriesTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .category_item:
            return DashboardCategoryItemTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .search:
            return DashboardSearchTableViewCell.getHeight(tableSize: tableView.frame.size)
        }
    }
}

// MARK: OffersProviderDelegate
extension DashboardViewController: OffersProviderDelegate {
    func offersProviderSuccess(offers: [Offer]) {
        self.dismissGlobalLoader()
        
        if offers.count > 0 {
            let controller = OffersViewController.controller
            controller.type = .offers
            controller.offers = offers
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            self.showMessage(
                header: "Informacja",
                message: "Brak ofert spełniających podane kryteria",
                primaryLabel: "OK",
                primaryAction: { })
        }
    }
    
    func offersProviderError(error: OffersProviderError, message: String?) {
        self.dismissGlobalLoader()
        self.showError(message: message, dismissText: "OK", dismiss: { })
    }
}

