//
//  MapViewController.swift
//  Petsin
//
//  Created by Mac on 19.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import MapKit

class MKPinAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String?, subtitle: String?, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}

class MapViewController: UIViewControllerBase {
    class var controller: MapViewController {
        return UIStoryboard(name: "Map", bundle: nil).instantiateViewController(withIdentifier: "Map") as! MapViewController
    }
    class func controller(with coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?) -> MapViewController {
        let controller = MapViewController.controller
        controller.item_coordinate = coordinate
        controller.item_title = title
        controller.item_subtitle = subtitle
        return controller
    }
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet { self.mapView.delegate = self }
    }
    
    var item_coordinate: CLLocationCoordinate2D?
    var item_title: String?
    var item_subtitle: String?
    
    // prepare view
    override func prepareView() {
        super.prepareView()
        
        // load map
        self.loadMap()
    }
    
    override func setNavigationBar() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
    }
    
    @IBAction func navigateClick(_ sender: UIButton) {
        self.navigate()
    }
    
    // load map
    fileprivate func loadMap() {
        guard let coordinate = self.item_coordinate else { return }
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(
            coordinate,
            regionRadius * 2.0,
            regionRadius * 2.0)
        self.mapView.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPinAnnotation(
            title: self.item_title,
            subtitle: self.item_subtitle,
            coordinate: coordinate)
        self.mapView.addAnnotation(annotation)
    }
    
    // navigate
    fileprivate func navigate() {
        guard let coordinate = self.item_coordinate else { return }
        
        let regionDistance: CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.item_title
        mapItem.openInMaps(launchOptions: options)
    }
}

// MARK: MKMapViewDelegate
extension MapViewController: MKMapViewDelegate {
    
    // pin view
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var view: MKPinAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
            view.canShowCallout = true
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    // pin details
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        self.showMessage(
            header: "Informacja",
            message: "Czy chcesz uruchomić nawigację do wskazanego miejsca?",
            primaryLabel: "Tak",
            secondaryLabel: "Nie",
            primaryAction: { [weak self] in
                guard let `self` = self else { return }
                self.navigate()
            }, secondaryAction: {
        })
    }
}

