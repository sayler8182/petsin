//
//  OfferViewController.swift
//  Petsin
//
//  Created by Mac on 16.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import CoreLocation

enum OfferCellType {
    case contact
    case categories
    case address
    case details
}

class OfferViewController: UIViewControllerBase {
    class var controller: OfferViewController {
        return UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "Offer") as! OfferViewController
    }
    
    fileprivate let headerHeight: CGFloat = 200
    
    @IBOutlet weak var headerView: OfferGalleryView!
    @IBOutlet weak var favouriteButton: UIButton! {
        didSet { self.favouriteButton.isHidden = true }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            OfferContactTableViewCell.register(tableView: self.tableView)
            OfferCategoriesTableViewCell.register(tableView: self.tableView)
            OfferAddressTableViewCell.register(tableView: self.tableView)
            OfferDetailsTableViewCell.register(tableView: self.tableView)
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint! {
        didSet { self.headerViewHeightConstraint.constant = self.headerHeight }
    }
    @IBOutlet weak var headerViewTopConstraint: NSLayoutConstraint! {
        didSet { self.headerViewTopConstraint.constant = 0 }
    }
    
    var offer_id: Int?
    fileprivate lazy var offersProvider: OffersProvider = OffersProvider(delegate: self)
    fileprivate var favourites_ids: [Int] = Storage.instance.favourites_ids
    fileprivate var offer: Offer? {
        didSet {
            
            // details
            self.offer_details = self.offer?.details_items ?? []
            
            // favourite
            self.favouriteButton.isHidden = false
            self.isOfferFavourite = self.favourites_ids.contains(where: { $0 == self.offer?.id })
            
            // images
            let images_src = self.offer?.images_src ?? []
            let hasImages = images_src.count > 0
            self.headerView.items = images_src
            let headerHeight: CGFloat = hasImages ? self.headerHeight : 0
            self.tableView.contentInset = UIEdgeInsetsMake(headerHeight, 0, 200, 0)
            self.headerViewHeightConstraint.constant = headerHeight
            self.view.layoutIfNeeded()
            
            // title
            let name = self.offer?.name
            let hasName = name?.isEmpty == false
            if hasName == true {
                self.navigationItem.titleView = nil
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
                self.navigationItem.title = name
                
            }
        }
    }
    fileprivate var items: [OfferCellType] = [] {
        didSet { self.tableView.reloadData() }
    }
    fileprivate var offer_details: [OfferDetails] = []
    fileprivate var ofeer_details_offset: Int = 0
    var isOfferFavourite: Bool = false {
        didSet {
            self.favouriteButton.isSelected = self.isOfferFavourite
            self.favouriteButton.imageView?.tintColor = self.isOfferFavourite ? Resources.lightGreen : UIColor.darkGray
        }
    }
    
    // prepare view
    override func prepareView() {
        super.prepareView()
        
        // load offer
        self.loadOffer()
        
        // favourite
        self.favouriteButton.setImage(UIImage(named: "favourite")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.favouriteButton.tintColor = UIColor.clear
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // start gallery animation
        self.headerView.startAnimation()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // stop gallery animation
        self.headerView.stopAnimation()
    }
    
    override func setNavigationBar() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
    }
    
    @IBAction func menuClick(_ sender: UIBarButtonItem) {
        self.showMenu(sender: sender)
    }
    
    @IBAction func shareClick(_ sender: UIBarButtonItem) {
        var items: [Any] = []
        if let shareText = self.offer?.shareText {
            items.append(shareText)
        }
        if let shareUrl = self.offer?.url_share_src {
            items.append(shareUrl)
        }
        if let image = self.headerView.imageToShare {
            items.append(image)
        }
        let activity = UIActivityViewController(activityItems: items, applicationActivities: nil)

        if let popoverController = activity.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        self.present(activity, animated: true, completion: nil)
    }
    
    @IBAction func favouriteClick(_ sender: UIButton) {
        guard let offer = self.offer else { return }
        let offer_id = offer.id
        
        // favourites ids
        if let index = self.favourites_ids.index(where: { $0 == offer_id }) {
            self.favourites_ids.remove(at: index)
            self.isOfferFavourite = false
        } else {
            self.favourites_ids.append(offer_id)
            self.isOfferFavourite = true
        }
        Storage.instance.favourites_ids = self.favourites_ids
        
        // favourites
        var favourites = Storage.instance.favourites
        if let index = favourites.index(where: { $0.id == offer_id }) {
            favourites.remove(at: index)
        } else {
            favourites.append(offer)
        }
        Storage.instance.favourites = favourites
    }
    
    // load offer
    fileprivate func loadOffer() {
        if let offer_id = self.offer_id {
            self.showGlobalLoader()
            self.offersProvider.offer(offer_id: offer_id)
        } else {
            self.showError(
                message: "Nie odnaleziono oferty.",
                dismissText: "OK",
                dismiss: { [weak self] in
                    guard let `self` = self else { return }
                    self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    // laod items
    fileprivate func loadItems() {
        guard let _ = self.offer else { return }
        var items: [OfferCellType] = []
        items.append(.contact)
        items.append(.categories)
        items.append(.address)
        
        // details
        self.ofeer_details_offset = items.count
        self.offer_details.forEach { _ in
            items.append(.details)
        }
        
        self.items = items
    }
}

// MARK: OfferContactTableViewCellDelegate
extension OfferViewController: OfferContactTableViewCellDelegate {
    func offerContactTableViewCellPhone(cell: OfferContactTableViewCell, phone: String) {
        guard let url = URL(string: "tel://\(phone)") else { return }
        guard UIApplication.shared.canOpenURL(url) == true else { return }
        UIApplication.shared.openURL(url)
    }
    
    func offerContactTableViewCellEmail(cell: OfferContactTableViewCell, email: String) {
        guard let url = URL(string: "mailto:\(email)") else { return }
        guard UIApplication.shared.canOpenURL(url) == true else { return }
        UIApplication.shared.openURL(url)
    }
}

// MARK: OfferCategoriesTableViewCellDelegate
extension OfferViewController: OfferCategoriesTableViewCellDelegate {
    
}

// MARK: OfferAddressTableViewCellDelegate
extension OfferViewController: OfferAddressTableViewCellDelegate {
    func offerAddressTableViewCellPhone(cell: OfferAddressTableViewCell, phone: String) {
        guard let url = URL(string: "tel://\(phone)") else { return }
        guard UIApplication.shared.canOpenURL(url) == true else { return }
        UIApplication.shared.openURL(url)
    }
    
    func offerAddressTableViewCellEmail(cell: OfferAddressTableViewCell, email: String) {
        guard let url = URL(string: "mailto:\(email)") else { return }
        guard UIApplication.shared.canOpenURL(url) == true else { return }
        UIApplication.shared.openURL(url)
    }
    
    func offerAddressTableViewCellMap(cell: OfferAddressTableViewCell, coordinate: CLLocationCoordinate2D) {
        let controller = MapViewController.controller(with: coordinate, title: self.offer?.name, subtitle: self.offer?.address)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension OfferViewController: UITableViewDelegate, UITableViewDataSource {
    
    // cell count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // cell click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let item = self.items[indexPath.row]
        switch item {
        case .contact: break
        case .categories: break
        case .address: break
        case .details:
            let details = self.offer_details[indexPath.row - self.ofeer_details_offset]
            details.is_selected = !details.is_selected
            
            UIView.performWithoutAnimation {
                self.tableView.beginUpdates()
                self.tableView.reloadRows(at: [indexPath], with: .none)
                self.tableView.endUpdates()
            }
        }
    }
    
    // cell view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.items[indexPath.row]
        switch item {
        case .contact:
            return tableView.dequeueReusableCell(withIdentifier: OfferContactTableViewCell.reuseIdentifier, for: indexPath)
        case .categories:
            return tableView.dequeueReusableCell(withIdentifier: OfferCategoriesTableViewCell.reuseIdentifier, for: indexPath)
        case .address:
            return tableView.dequeueReusableCell(withIdentifier: OfferAddressTableViewCell.reuseIdentifier, for: indexPath)
        case .details:
            return tableView.dequeueReusableCell(withIdentifier: OfferDetailsTableViewCell.reuseIdentifier, for: indexPath)
        }
    }
    
    // cell will appear
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let item = self.items[indexPath.row]
        switch item {
        case .contact:
            let cell = cell as! OfferContactTableViewCell
            cell.delegate = self
            cell.offer = self.offer
        case .categories:
            let cell = cell as! OfferCategoriesTableViewCell
            cell.delegate = self
            cell.offer = self.offer
        case .address:
            let cell = cell as! OfferAddressTableViewCell
            cell.delegate = self
            cell.offer = self.offer
        case .details:
            let cell = cell as! OfferDetailsTableViewCell
            cell.details = self.offer_details[indexPath.row - ofeer_details_offset]
        }
    }
    
    // cell end displaying
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! UITableViewCellBase
        cell.endDisplaying()
    }
    
    // cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = self.items[indexPath.row]
        switch item {
        case .contact:
            return OfferContactTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .categories:
            return OfferCategoriesTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .address:
            return OfferAddressTableViewCell.getHeight(tableSize: tableView.frame.size)
        case .details:
            let details = self.offer_details[indexPath.row - ofeer_details_offset]
            return OfferDetailsTableViewCell.getHeight(tableSize: tableView.frame.size, details: details)
        }
    }
}

// MARK: UIScreollViewDelegate
extension OfferViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        var topValue = -(self.headerHeight + offsetY)
        topValue = max(-self.headerHeight, topValue)
        topValue = min(topValue, 0)
        if self.headerViewTopConstraint.constant != topValue {
            self.headerViewTopConstraint.constant = topValue
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: OffersProviderDelegate
extension OfferViewController: OffersProviderDelegate {
    func offersProviderSuccess(offer: Offer) {
        self.dismissGlobalLoader()
        
        // save offer
        self.offer = offer
        
        // load items
        self.loadItems()
    }
    
    func offersProviderError(error: OffersProviderError, message: String?) {
        self.dismissGlobalLoader()
        self.showError(
            message: message,
            dismissText: "OK",
            dismiss: { [weak self] in
                guard let `self` = self else { return }
                self.navigationController?.popViewController(animated: true)
            })
    }
}
