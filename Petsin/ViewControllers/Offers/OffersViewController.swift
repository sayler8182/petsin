//
//  OffersViewController.swift
//  Petsin
//
//  Created by Mac on 13.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

enum OffersType {
    case offers
    case favourites
}

class OffersViewController: UIViewControllerBase {
    class var controller: OffersViewController {
        return UIStoryboard(name: "Offers", bundle: nil).instantiateViewController(withIdentifier: "Offers") as! OffersViewController
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            OffersTableViewCell.register(tableView: self.tableView)
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    var offers: [Offer] = [] {
        // willSet { self.offers = newValue.sorted(by: { $0.is_promotion }) }
        didSet { self.tableView?.reloadData() }
    }
    var type: OffersType = .offers
    fileprivate var sort: Sort = Sort.default { didSet { self.sortItems() } }
    fileprivate var favourites_ids: [Int] = []
    
    // prepare view
    override func prepareView() {
        super.prepareView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // refresh items
        self.favourites_ids = Storage.instance.favourites_ids
        
        // sort items
        self.sortItems()
    }
    
    override func setNavigationBar() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
    }
    
    @IBAction func menuClick(_ sender: UIBarButtonItem) {
        self.showMenu(sender: sender)
    }
    
    @IBAction func filterClick(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let priceAscAction = UIAlertAction(title: "Cena rosnąco", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.sort = .priceAsc
        })
        let priceDescAction = UIAlertAction(title: "Cena malejąco", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.sort = .priceDesc
        })
        let nameAscAction = UIAlertAction(title: "Nazwa rosnąco", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.sort = .nameAsc
        })
        let nameDescAction = UIAlertAction(title: "Nazwa malejąco", style: .default, handler: { [weak self] (alert: UIAlertAction!) -> Void in
            guard let `self` = self else { return }
            self.sort = .nameDesc
        })
        let cancelAction = UIAlertAction(title: "Anuluj", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
        })
        
        
        alertController.addAction(priceAscAction)
        alertController.addAction(priceDescAction)
        alertController.addAction(nameAscAction)
        alertController.addAction(nameDescAction)
        alertController.addAction(cancelAction)
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // sort items
    fileprivate func sortItems() {
        switch self.sort {
        case .priceAsc:
            self.offers = self.offers.sorted(by: { ($0.price_from ?? 0) < ($1.price_from ?? 0) })
        case .priceDesc:
            self.offers = self.offers.sorted(by: { ($0.price_from ?? 0) > ($1.price_from ?? 0) })
        case .nameAsc:
            self.offers = self.offers.sorted(by: { $0.name.localizedCaseInsensitiveCompare($1.name) == .orderedAscending })
        case .nameDesc:
            self.offers = self.offers.sorted(by: { $0.name.localizedCaseInsensitiveCompare($1.name) == .orderedDescending })
        }
    }
}

// MARK: OffersTableViewCellDelegate
extension OffersViewController: OffersTableViewCellDelegate {
    func offersTableViewCellFavourite(cell: OffersTableViewCell, offer_id: Int) {
        
        // favourites ids
        if let index = self.favourites_ids.index(where: { $0 == offer_id }) {
            self.favourites_ids.remove(at: index)
            cell.isOfferFavourite = false
        } else {
            self.favourites_ids.append(offer_id)
            cell.isOfferFavourite = true
        }
        Storage.instance.favourites_ids = self.favourites_ids

        // favourites
        var favourites = Storage.instance.favourites
        if let index = favourites.index(where: { $0.id == offer_id }) {
            favourites.remove(at: index)
        } else if let offer = self.offers.first(where: { $0.id == offer_id }) {
            favourites.append(offer)
        }
        Storage.instance.favourites = favourites
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension OffersViewController: UITableViewDelegate, UITableViewDataSource {
    
    // cell count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offers.count
    }
    
    // cell click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let controller = OfferViewController.controller
        controller.offer_id = self.offers[indexPath.row].id
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // cell view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: OffersTableViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let offer = self.offers[indexPath.row]
        let cell = cell as! OffersTableViewCell
        cell.delegate = self
        cell.offer = offer
        cell.isOfferFavourite = self.favourites_ids.contains(offer.id)
    }
    
    // cell end displaying
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! UITableViewCellBase
        cell.endDisplaying()
    }
    
    // cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return OffersTableViewCell.getHeight(tableSize: tableView.frame.size)
    }
}
