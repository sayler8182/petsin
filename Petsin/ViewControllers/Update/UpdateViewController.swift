//
//  UpdateViewController.swift
//  Petsin
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class UpdateViewController: UIViewControllerBase {
    class var controller: UpdateViewController {
        return UIStoryboard(name: "Update", bundle: nil).instantiateViewController(withIdentifier: "Update") as! UpdateViewController
    }
    
    fileprivate lazy var queue: Queue = Queue()
    fileprivate lazy var catgoriesProvider: CategoriesProvider = CategoriesProvider(delegate: self)
    fileprivate lazy var locationsProvider: LocationsProvider = LocationsProvider(delegate: self)
    fileprivate lazy var typesProvider: TypesProvider = TypesProvider(delegate: self)
    
    // prepare view
    override func prepareView() {
        super.prepareView()
        
        // begin update
        self.beginUpdate()
    } 
    
    // begin update
    fileprivate func beginUpdate() {
        
        self.queue.add {
            if Storage.instance.categories.count <= 0 {
                self.catgoriesProvider.categories()
            } else {
                self.queue.beginNext()
            }
        }
        self.queue.add {
            let _cities = Storage.instance._cities
            if _cities.count <= 0 {
                self.locationsProvider.cities()
            } else {
                Storage.instance.cities = _cities
                self.queue.beginNext()
            }
        }
        self.queue.add {
            if Storage.instance.types.count <= 0 {
                self.typesProvider.types()
            } else {
                self.queue.beginNext()
            }
        }
        self.queue.setEnd { [weak self] (success, message) in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.queue.disposeActions()
                if success {
                    self.navigationController?.isNavigationBarHidden = false
                    self.navigationController?.pushViewController(DashboardViewController.controller, animated: true)
                } else {
                    self.showError(
                        message: message,
                        dismissText: "OK",
                        dismiss: {
                            exit(0)
                    }, retry: {
                        self.beginUpdate()
                    })
                }
            }
        }
        self.queue.begin()
    }
}

// MARK: - CategoriesProviderDelegate
extension UpdateViewController: CategoriesProviderDelegate {
    func categoriesProviderSuccess(categories: [Category]) {
        self.queue.beginNext()
    }
    
    func categoriesProviderError(error: CategoriesProviderError, message: String?) {
        self.queue.error(message: message)
    }
}

// MARK: - LocationsProviderDelegate
extension UpdateViewController: LocationsProviderDelegate {
    func locationsProviderSuccess(cities: [City]) {
        self.queue.beginNext()
    }
    
    func locationsProviderError(error: LocationsProviderError, message: String?) {
        self.queue.error(message: message)
    }
}

// MARK: - TypesProviderDelegate
extension UpdateViewController: TypesProviderDelegate {
    func typesProviderSuccess(types: [Type]) {
        self.queue.beginNext()
    }
    
    func typesProviderError(error: TypesProviderError, message: String?) {
        self.queue.error(message: message)
    }
}
