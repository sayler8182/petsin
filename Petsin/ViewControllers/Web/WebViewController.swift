//
//  WebViewController.swift
//  Petsin
//
//  Created by Mac on 11.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class WebViewController: UIViewControllerBase {
    class var controller: WebViewController {
        return UIStoryboard(name: "Web", bundle: nil).instantiateViewController(withIdentifier: "Web") as! WebViewController
    }
    class func controller(with url: URL?) -> WebViewController {
        let controller = WebViewController.controller
        controller.url = url
        return controller
    }
    
    @IBOutlet weak var webView: UIWebView! {
        didSet { self.webView.delegate = self }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var url: URL?
    
    // prepare view
    override func prepareView() {
        super.prepareView()
        
        // load website
        self.loadWebSite()
    }
    
    override func setNavigationBar() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "logo"))
    } 
    
    // load website
    fileprivate func loadWebSite() {
        guard let url = self.url else { return }
        let request = URLRequest(url: url)
        self.webView.loadRequest(request)
    }
}

// MARK: UIWebViewDelegate
extension WebViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
}
