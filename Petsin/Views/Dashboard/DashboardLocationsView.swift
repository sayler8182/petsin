//
//  DashboardLocationsView.swift
//  Petsin
//
//  Created by Mac on 16.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class DashboardLocationsView: UIViewXib {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            DashboardLocationsTableViewCell.register(tableView: self.tableView)
        }
    }
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: DashboardLocationsViewDelegate?
    var query: String? = nil {
        didSet { self.filterCities() }
    }
    var cities: [City] = [] {
        didSet { self.filterCities() }
    }
    var filteredCities: [City] = [] {
        didSet { self.tableView.reloadData() }
    }
    
    override func prepareView() {
        super.prepareView()
        
        self.backgroundColor = UIColor.clear
        self.xib.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // shadow
        self.tableView.superview?.layer.shadow(radius: 3, opacity: 0.2)
    }
    
    // show view
    func showView(superview: UIView?, origin: CGPoint, animated: Bool) {
        guard let superview = superview else { return }
        self.layer.removeAllAnimations()
        
        if self.superview == nil {
            self.alpha = 0
            
            // create first time
            superview.addSubview(self)
            
            // constraint
            let height: CGFloat = 200
            self.snp.makeConstraints { (make) in
                make.leading.equalTo(superview.snp.leading).offset(8)
                make.top.equalTo(superview.snp.top).offset(origin.y - height + 50)
                make.height.equalTo(height)
                make.width.equalTo(UIScreen.main.bounds.width - 74)
            }
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        
        // animation
        if animated {
            UIView.animate(
                withDuration: 0.7,
                animations: {
                    self.alpha = 1
            })
        } else {
            self.alpha = 1
        }
    }
    
    // filter cities
    fileprivate func filterCities() {
        var cities = self.cities
        if let query = self.query, query.isEmpty == false {
            cities = cities.filter {
                $0.name.localizedCaseInsensitiveContains(query)
            }
        }
        self.filteredCities = cities
        
        // update height
        let count = CGFloat(self.filteredCities.count)
        let height = DashboardLocationsTableViewCell.getHeight(tableSize: self.tableView.frame.size)
        self.tableHeightConstraint.constant = min(200, count * height)
        self.setNeedsDisplay()
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension DashboardLocationsView: UITableViewDelegate, UITableViewDataSource {
    
    // cell count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredCities.count
    }
    
    // cell click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let city = self.filteredCities[indexPath.row]
        self.delegate?.dashboardLocationsViewSelected(city: city)
    }
    
    // cell view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: DashboardLocationsTableViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! DashboardLocationsTableViewCell
        cell.city = self.filteredCities[indexPath.row]
    }
    
    // cell height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DashboardLocationsTableViewCell.getHeight(tableSize: tableView.frame.size)
    }
}
