//
//  DashboardLocationsViewDelegate.swift
//  Petsin
//
//  Created by Mac on 16.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation

protocol DashboardLocationsViewDelegate: class {
    func dashboardLocationsViewSelected(city: City)
}
