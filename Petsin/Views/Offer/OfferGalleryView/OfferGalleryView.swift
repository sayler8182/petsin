//
//  OfferGalleryView.swift
//  Petsin
//
//  Created by Mac on 17.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit

class OfferGalleryView: UIViewXib {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            OfferGalleryCollectionViewCell.register(collectionView: self.collectionView)
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!
    
    fileprivate var timer: Timer?
    fileprivate var page: Int = 0
    var items: [URL] = [] {
        didSet {
            self.collectionView.reloadData()
            self.pageControl.numberOfPages = self.items.count
        }
    }
    var imageToShare: UIImage? {
        return self.collectionView.visibleCells
            .flatMap { $0 as? OfferGalleryCollectionViewCell }
            .flatMap { $0.imageToShare }
            .first 
    }
    
    // starts gallery animation
    func startAnimation() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerTick(_:)), userInfo: nil, repeats: true)
    }
    
    // stops gallery animation
    func stopAnimation() {
        self.timer?.invalidate()
    }
    
    @objc func timerTick(_ timer: Timer) {
        var new_page = self.page + 1
        if new_page >= self.items.count {
            new_page = 0
        }
        
        // scroll to page
        let width = self.collectionView.frame.size.width
        let offsetX = width * CGFloat(new_page)
        self.collectionView.scrollRectToVisible(CGRect(origin: CGPoint(x: offsetX, y: 0), size: self.collectionView.frame.size), animated: true)
    }
}

// MARK: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension OfferGalleryView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // cell count
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // cell view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: OfferGalleryCollectionViewCell.reuseIdentifier, for: indexPath)
    }
    
    // cell will appear
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! OfferGalleryCollectionViewCell
        cell.item = self.items[indexPath.row]
    }
    
    // cell end displaying
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as! UICollectionViewCellBase
        cell.endDisplaying()
    }
    
    
    // cell size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return OfferGalleryCollectionViewCell.getSize(collectionSize: collectionView.frame.size)
    }
}

// MARK: UIScrollViewDelegate
extension OfferGalleryView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetX = scrollView.contentOffset.x
        let width = scrollView.frame.width
        var index = Int((offsetX / width) + 0.5)
        index = max(0, index)
        index = min(index, self.items.count)
        if index != self.pageControl.currentPage {
            self.page = index
            self.pageControl.currentPage = index
        }
    }
}
