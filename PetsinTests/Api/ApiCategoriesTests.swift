//
//  ApiCategoriesTests.swift
//  PetsinTests
//
//  Created by Mac on 10.10.2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import XCTest
@testable import Petsin_LOCAL

class ApiCategoriesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() { 
        super.tearDown()
    }
    
    func testCategories() {
        let expectation = self.expectation(description: "categories")
        let apiCategories = ApiCategories()
        let _  = apiCategories.categoriesRequest(
            success: { (categories) in
                print("Categories: \(categories.count)")
                XCTAssertTrue(categories.count > 0)
        }, error: { (message, result) in
            XCTFail(message)
        }, failure: {
            XCTFail()
        }, cancelled: {
            XCTFail()
        })
        self.wait(for: [expectation], timeout: 5)
    }
}

